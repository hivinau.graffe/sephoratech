// Copyright © Hivinau GRAFFE. All rights reserved.

import Swinject
import SwinjectAutoregistration

struct DomainsAssembly: Assembly {
    
    // MARK: - Properties
    
    let assembler: Assembler
    
    // MARK: - Assembly methods
    
    func assemble(container: Container) {
        assembler.apply(assemblies: [
            ProductsAssembly(),
            ProductDetailsAssembly()
        ])
    }
}
