// Copyright © Hivinau GRAFFE. All rights reserved.

import Action
import Domain
import RxSwift

// sourcery: AutoMockable
protocol ProductsViewModel: AnyObject, ViewModel {
    var title: Observable<String?> { get }
    var cellTypes: Observable<[CellType]> { get }
    var selectedCellType: Binder<CellType> { get }
    var selectProduct: Action<Product, Void>? { get set }
}
