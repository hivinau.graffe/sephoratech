// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain

// sourcery: AutoMockable
protocol ProductCellViewModelProvider {
    
    func provide(_ product: Product) -> ProductCellViewModel
}
