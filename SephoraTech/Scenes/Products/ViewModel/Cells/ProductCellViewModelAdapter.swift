// Copyright © Hivinau GRAFFE. All rights reserved.

import Action
import Domain
import RxCocoa
import RxSwift
import UIKit

final class ProductCellViewModelAdapter: NSObject, ProductCellViewModel {
    
    // MARK: - ProductCellViewModel properties
    
    let product: Product
    
    lazy var productName = productSubject
        .asObservable()
        .map(\.name)
    
    lazy var productDescription = productSubject
        .asObservable()
        .map(\.descriptionFr)
    
    lazy var productPrice = productSubject
        .asObservable()
        .map(\.price)
        .map(priceFormatter.format(_:))
    
    lazy var productImage = productSubject
        .asObservable()
        .compactMap(\.imagesUrl?.small)
        .compactMap(imagesDownloader.image(for:))
        .switchLatest()
    
    lazy var isProductSpecialBrand = productSubject
        .asObservable()
        .map(\.isSpecialBrand)
    
    // MARK: - Private properties
    
    private let imagesDownloader: ImagesDownloader
    private let priceFormatter: PriceFormatter
    private let productSubject: BehaviorSubject<Product>
    
    // MARK: - Init
    
    init(imagesDownloader: ImagesDownloader,
         priceFormatter: PriceFormatter,
         product: Product) {
        self.imagesDownloader = imagesDownloader
        self.priceFormatter = priceFormatter
        self.product = product
        self.productSubject = BehaviorSubject<Product>(value: self.product)
        super.init()
    }
}

