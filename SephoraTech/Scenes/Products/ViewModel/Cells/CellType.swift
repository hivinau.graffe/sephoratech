// Copyright © Hivinau GRAFFE. All rights reserved.

import UIKit

enum CellType {
    case none
    case product(ProductCellViewModel)
}

