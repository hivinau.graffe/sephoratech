// Copyright © Hivinau GRAFFE. All rights reserved.

import Action
import Domain
import RxSwift
import UIKit

// sourcery: AutoMockable
protocol ProductCellViewModel: ViewModel {
    var product: Product { get }
    var productName: Observable<String?> { get }
    var productDescription: Observable<String?> { get }
    var productPrice: Observable<String?> { get }
    var productImage: Observable<UIImage?> { get }
    var isProductSpecialBrand: Observable<Bool> { get }
}
