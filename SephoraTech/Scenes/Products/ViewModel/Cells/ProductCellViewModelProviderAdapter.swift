// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Swinject
import SwinjectAutoregistration

struct ProductCellViewModelProviderAdapter: ProductCellViewModelProvider {
    
    // MARK: - Private properties
    
    private let resolver: Resolver
    
    // MARK: - Init
    
    init(resolver: Resolver) {
        self.resolver = resolver
    }
    
    // MARK: - ProductCellViewModelProvider methods
    
    func provide(_ product: Product) -> ProductCellViewModel {
        
        return resolver~>(ProductCellViewModel.self, argument: product)
    }
}
