// Copyright © Hivinau GRAFFE. All rights reserved.

import Action
import Domain
import Foundation
import RxCocoa
import RxSwift

final class ProductsViewModelAdapter: NSObject, ProductsViewModel {
    
    // MARK: - ProductsViewModel Properties
    
    lazy var title = Observable<String?>.just(appName)
    lazy var cellTypes = cellTypesSubject.asObservable()
    
    var selectProduct: Action<Product, Void>?
    
    var selectedCellType: Binder<CellType> {
        Binder(self) {
            switch $1 {
            case .product(let viewModel): $0.selectProduct?.execute(viewModel.product)
            default: break
            }
        }
    }
    
    // MARK: - Private properties
    
    private let appName: String
    private let fetchProductsUseCase: FetchProductsUseCase
    private let productCellViewModelProvider: ProductCellViewModelProvider
    private let disposeBag = DisposeBag()
    private let cellTypesSubject = ReplaySubject<[CellType]>.create(bufferSize: 1)
    
    // MARK: - Init
    
    init(appName: String,
         fetchProductsUseCase: FetchProductsUseCase,
         productCellViewModelProvider: ProductCellViewModelProvider) {
        self.appName = appName
        self.fetchProductsUseCase = fetchProductsUseCase
        self.productCellViewModelProvider = productCellViewModelProvider
        super.init()
        
        bindObservers()
    }
    
    // MARK: - Private methods
    
    private func bindObservers() {
        
        fetchProductsUseCase
            .start()
            .asObservable()
            .map { [weak self] products in
                guard let self else { return [] }
                
                return products
                    .map {
                        .product(self.productCellViewModelProvider.provide($0))
                    }
            }
            .bind(to: cellTypesSubject)
            .disposed(by: disposeBag)
    }
}
