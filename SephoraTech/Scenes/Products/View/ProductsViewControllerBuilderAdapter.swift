// Copyright © Hivinau GRAFFE. All rights reserved.

import UIKit
import Swinject
import SwinjectAutoregistration

struct ProductsViewControllerBuilderAdapter: ProductsViewControllerBuilder {
    
    // MARK: - Private properties
    
    private let resolver: Resolver
    
    // MARK: - Init
    
    init(resolver: Resolver) {
        self.resolver = resolver
    }
    
    // MARK: - ViewControllerFactory methods
    
    func build() -> ProductsViewController? {
        
        return resolver~>
    }
}
