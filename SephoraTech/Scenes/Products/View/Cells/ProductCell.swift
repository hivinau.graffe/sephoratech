// Copyright © Hivinau GRAFFE. All rights reserved.

import RxCocoa
import RxSwift
import TypographyKit
import UIKit

final class ProductCell: BaseTableViewCell<ProductCellViewModel> {
    
    // MARK: - Outlets
    
    @IBOutlet
    var cardView: UIView! {
        didSet {
            cardView.backgroundColor = .backgroundSecondaryColor
            cardView.layer.shadowColor = UIColor.shadowColor?.cgColor
            cardView.layer.cornerRadius = 4
            cardView.layer.shadowOffset = .init(width: 0, height: 0)
            cardView.layer.shadowOpacity = 0.4
            cardView.layer.shadowRadius = 5
        }
    }
    
    @IBOutlet
    var priceView: UIView! {
        didSet {
            priceView.backgroundColor = .backgroundTertiaryColor
        }
    }
    
    @IBOutlet
    var productSpecialImageView: UIImageView! {
        didSet {
            productSpecialImageView.image = UIImage(systemName: "sparkles")?.withRenderingMode(.alwaysTemplate)
            productSpecialImageView.tintColor = .backgroundTertiaryColor
            productSpecialImageView.contentMode = .scaleAspectFit
        }
    }
    
    @IBOutlet
    var productImageView: UIImageView! {
        didSet {
            productImageView.contentMode = .scaleAspectFill
        }
    }
    
    @IBOutlet
    var productNameLabel: UILabel! {
        didSet {
            productNameLabel.numberOfLines = 0
            productNameLabel.textColor = .primaryColor
            productNameLabel.fontTextStyle = .headline
        }
    }
    
    @IBOutlet
    var productDescriptionLabel: UILabel! {
        didSet {
            productDescriptionLabel.numberOfLines = 0
            productDescriptionLabel.textColor = .primaryColor
            productDescriptionLabel.fontTextStyle = .body
        }
    }
    
    @IBOutlet
    var productPriceLabel: UILabel! {
        didSet {
            productPriceLabel.numberOfLines = 1
            productPriceLabel.textColor = .secondaryColor
            productPriceLabel.fontTextStyle = .title3
        }
    }
    
    // MARK: - BaseTableViewCell properties
    
    override var viewModel: ProductCellViewModel? {
        didSet {
            bindObservers()
        }
    }
    
    // MARK: - Private properties
    
    private var disposeBag = DisposeBag()
    
    // MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        cardView.layer.shadowColor = UIColor.shadowColor?.cgColor
        disposeBag = DisposeBag()
    }
    
    // MARK: - Private methods
    
    private func bindObservers() {
        viewModel?.productName
            .asDriver(onErrorJustReturn: nil)
            .drive(productNameLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel?.productDescription
            .asDriver(onErrorJustReturn: nil)
            .drive(productDescriptionLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel?.productPrice
            .asDriver(onErrorJustReturn: nil)
            .drive(productPriceLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel?.productImage
            .asDriver(onErrorJustReturn: nil)
            .drive(productImageView.rx.image)
            .disposed(by: disposeBag)
        
        viewModel?.isProductSpecialBrand
            .map(!)
            .asDriver(onErrorJustReturn: false)
            .drive(productSpecialImageView.rx.isHidden)
            .disposed(by: disposeBag)
        
        rx.setHighlighted
            .map { $0 ? 0 : 5 }
            .asDriver(onErrorJustReturn: 5)
            .drive(cardView.rx.shadowRadius)
            .disposed(by: disposeBag)
    }
}
