// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

protocol ProductsViewControllerBuilder: ViewControllerBuilder where ViewController == ProductsViewController {
}
