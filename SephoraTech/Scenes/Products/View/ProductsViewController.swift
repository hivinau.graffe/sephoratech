// Copyright © Hivinau GRAFFE. All rights reserved.

import RxCocoa
import RxSwift
import UIKit

final class ProductsViewController: BaseViewController<ProductsViewModel> {
    
    // MARK: - Constants
    
    private enum Constants {
        static let productCellIdentifier = "ProductCell"
    }
    
    // MARK: - Outlets
    
    @IBOutlet
    var tableView: UITableView! {
        didSet {
            let productNib = UINib(nibName: Constants.productCellIdentifier, bundle: .main)
            tableView.register(productNib, forCellReuseIdentifier: Constants.productCellIdentifier)
            tableView.separatorStyle = .none
            tableView.backgroundColor = .clear
            tableView.rowHeight = UITableView.automaticDimension
        }
    }
    
    override var view: UIView! {
        didSet {
            view.backgroundColor = .backgroundPrimaryColor
        }
    }
    
    // MARK: - Private properties
    
    private let disposeBag = DisposeBag()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindObservers()
    }
    
    // MARK: - Private methods
    
    private func bindObservers() {
        guard let viewModel = viewModel else { return }
        
        viewModel.title
            .asDriver(onErrorJustReturn: nil)
            .drive(rx.title)
            .disposed(by: disposeBag)
        
        Observable.combineLatest(rx.viewDidAppear,
                                 rx.traitCollectionDidChange.startWith(.current),
                                 viewModel.cellTypes)
        .map(\.2)
        .asDriver(onErrorJustReturn: [])
        .drive(tableView.rx.items(cellIdentifier: Constants.productCellIdentifier,
                                  cellType: ProductCell.self)) { _, cellType, cell in
            guard case .product(let productViewModel) = cellType else { return }
            
            cell.viewModel = productViewModel
        }
        .disposed(by: disposeBag)
        
        tableView.rx.modelSelected(CellType.self)
            .bind(to: viewModel.selectedCellType)
            .disposed(by: disposeBag)
    }
}
