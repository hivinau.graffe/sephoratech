// Copyright © Hivinau GRAFFE. All rights reserved.

import Action
import Domain
import RxSwift
import UIKit

final class ProductsCoordinatorAdapter: NSObject, ProductsCoordinator {
    
    // MARK: - Error
    
    private enum ProductsCoordinatorError: Error {
        case startFailed
    }
    
    // MARK: - Constants
    
    private enum Constants {
        static let textFont = UIFont(name: "GraphikApp-Semibold", size: 22)
        static let textColor = UIColor.primaryColor
    }
    
    // MARK: - ProductsCoordinator properties
    
    var window: UIWindowWrapper
    
    // MARK: - Private properties
    
    private let productDetailsCoordinator: ProductDetailsCoordinator
    private let productsViewControllerBuilder: any ProductsViewControllerBuilder
    private let productsRepository: ProductsRepository
    
    private var productsViewController: ProductsViewController? {
        productsViewControllerBuilder.build()
    }
    
    private lazy var selectProduct = Action<Product, Void> { [weak self] product in
        self?.productsRepository
            .selectProduct(product)
            .map { [weak self] _ in
                self?.productDetailsCoordinator.start()
            }
            .map { _ in () } ?? .empty()
    }
    
    // MARK: - Init
    
    init(window: UIWindowWrapper,
         productDetailsCoordinator: ProductDetailsCoordinator,
         productsViewControllerBuilder: any ProductsViewControllerBuilder,
         productsRepository: ProductsRepository) {
        self.window = window
        self.productDetailsCoordinator = productDetailsCoordinator
        self.productsViewControllerBuilder = productsViewControllerBuilder
        self.productsRepository = productsRepository
        super.init()
    }
    
    // MARK: - Coordinator methods
    
    func start() -> Completable {
        return showProductsViewController()
    }
    
    // MARK: - Private methods
    
    private func showProductsViewController() -> Completable {
        guard let productsViewController = productsViewController else {
            return .error(ProductsCoordinatorError.startFailed)
        }
        
        productsViewController.viewModel?.selectProduct = selectProduct
        
        let navigationController = UINavigationController(rootViewController: productsViewController)
        setupNavigationController(navigationController)
        window.rootViewController = navigationController
        
        window.makeKeyAndVisible()
        
        return .empty()
    }
    
    private func setupNavigationController(_ navigationController: UINavigationController) {
        navigationController.navigationBar.prefersLargeTitles = true
        guard let textFont = Constants.textFont,
              let textColor = Constants.textColor else { return }
        
        navigationController.navigationBar.titleTextAttributes = [
            .font: textFont,
            .foregroundColor: textColor
        ]
        
        navigationController.navigationBar.tintColor = textColor
    }
}
