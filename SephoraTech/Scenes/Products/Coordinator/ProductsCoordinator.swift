// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain

// sourcery: AutoMockable
protocol ProductsCoordinator: Coordinator {
    var window: UIWindowWrapper { get }
}
