// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Swinject
import SwinjectAutoregistration

struct ProductsAssembly: Assembly {
    
    // MARK: - Assembly methods
    
    func assemble(container: Container) {
        assembleProductsCoordinator(container: container)
        assembleProductsViewModel(container: container)
        assembleProductCellViewModel(container: container)
        assembleProductsViewController(container: container)
        assembleProductsViewControllerBuilder(container: container)
        assembleProductCellViewModelProvider(container: container)
    }
    
    // MARK: - Private methods
    
    private func assembleProductsCoordinator(container: Container) {
        container.autoregister(ProductsCoordinator.self, initializer: ProductsCoordinatorAdapter.init)
            .inObjectScope(.container)
    }
    
    private func assembleProductsViewController(container: Container) {
        container.register(ProductsViewController.self) { resolver in
            let productsViewController = ProductsViewController()
            productsViewController.viewModel = resolver~>
            return productsViewController
        }
        .inObjectScope(.container)
    }
    
    private func assembleProductsViewControllerBuilder(container: Container) {
        container.register((any ProductsViewControllerBuilder).self) {
            ProductsViewControllerBuilderAdapter(resolver: $0)
        }
        .inObjectScope(.container)
    }
    
    private func assembleProductsViewModel(container: Container) {
        container.register(ProductsViewModel.self) { resolver in
            return ProductsViewModelAdapter(appName: AppConfig.App.displayName,
                                            fetchProductsUseCase: resolver~>,
                                            productCellViewModelProvider: resolver~>)
        }
        .inObjectScope(.container)
    }
    
    private func assembleProductCellViewModelProvider(container: Container) {
        container.register(ProductCellViewModelProvider.self) {
            ProductCellViewModelProviderAdapter(resolver: $0)
        }
        .inObjectScope(.container)
    }
    
    private func assembleProductCellViewModel(container: Container) {
        container.register(ProductCellViewModel.self) { resolver, product in
            return ProductCellViewModelAdapter(imagesDownloader: resolver~>,
                                               priceFormatter: resolver~>,
                                               product: product)
        }
        .inObjectScope(.transient)
    }
}
