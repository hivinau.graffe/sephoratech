// Copyright © Hivinau GRAFFE. All rights reserved.

import RxCocoa
import RxSwift
import TypographyKit
import UIKit

final class ProductDetailsViewController: BaseViewController<ProductDetailsViewModel> {
    
    @IBOutlet
    var priceView: UIView! {
        didSet {
            priceView.backgroundColor = .backgroundTertiaryColor
        }
    }
    
    @IBOutlet
    var productSpecialImageView: UIImageView! {
        didSet {
            productSpecialImageView.image = UIImage(systemName: "sparkles")?.withRenderingMode(.alwaysTemplate)
            productSpecialImageView.tintColor = .backgroundTertiaryColor
            productSpecialImageView.contentMode = .scaleAspectFit
        }
    }
    
    @IBOutlet
    var productImageView: UIImageView! {
        didSet {
            productImageView.contentMode = .scaleAspectFill
        }
    }
    
    @IBOutlet
    var productNameLabel: UILabel! {
        didSet {
            productNameLabel.numberOfLines = 0
            productNameLabel.textColor = .primaryColor
            productNameLabel.fontTextStyle = .headline
        }
    }
    
    @IBOutlet
    var productDescriptionLabel: UILabel! {
        didSet {
            productDescriptionLabel.numberOfLines = 0
            productDescriptionLabel.textColor = .primaryColor
            productDescriptionLabel.fontTextStyle = .body
        }
    }
    
    @IBOutlet
    var productPriceLabel: UILabel! {
        didSet {
            productPriceLabel.numberOfLines = 1
            productPriceLabel.textColor = .secondaryColor
            productPriceLabel.fontTextStyle = .title3
        }
    }
    
    override var view: UIView! {
        didSet {
            view.backgroundColor = .backgroundSecondaryColor
        }
    }
    
    // MARK: - Private properties
    
    private var disposeBag = DisposeBag()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindObservers()
    }
    
    // MARK: - Private methods
    
    private func bindObservers() {
        guard let viewModel = viewModel else { return }
        
        viewModel.productName
            .asDriver(onErrorJustReturn: nil)
            .drive(productNameLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.productDescription
            .asDriver(onErrorJustReturn: nil)
            .drive(productDescriptionLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.productPrice
            .asDriver(onErrorJustReturn: nil)
            .drive(productPriceLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.productImage
            .asDriver(onErrorJustReturn: nil)
            .drive(productImageView.rx.image)
            .disposed(by: disposeBag)
        
        viewModel.isProductSpecialBrand
            .map(!)
            .asDriver(onErrorJustReturn: false)
            .drive(productSpecialImageView.rx.isHidden)
            .disposed(by: disposeBag)
        
        rx.viewDidDisappear
            .map { _ in () }
            .bind(to: viewModel.deselectProduct.inputs)
            .disposed(by: disposeBag)
    }
}
