// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

protocol ProductDetailsViewControllerBuilder: ViewControllerBuilder where ViewController == ProductDetailsViewController {
}
