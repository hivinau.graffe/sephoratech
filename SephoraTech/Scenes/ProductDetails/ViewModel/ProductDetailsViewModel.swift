// Copyright © Hivinau GRAFFE. All rights reserved.

import Action
import Domain
import RxSwift
import UIKit

// sourcery: AutoMockable
protocol ProductDetailsViewModel: ViewModel {
    var productName: Observable<String?> { get }
    var productDescription: Observable<String?> { get }
    var productPrice: Observable<String?> { get }
    var productImage: Observable<UIImage?> { get }
    var isProductSpecialBrand: Observable<Bool> { get }
    var deselectProduct: CocoaAction { get set }
}
