// Copyright © Hivinau GRAFFE. All rights reserved.

import Action
import Domain
import RxCocoa
import RxSwift
import UIKit

final class ProductDetailsViewModelAdapter: NSObject, ProductDetailsViewModel {
    
    // MARK: - ProductDetailsViewModel properties
    
    lazy var productName = productSubject
        .asObservable()
        .map(\.name)
    
    lazy var productDescription = productSubject
        .asObservable()
        .map(\.descriptionFr)
    
    lazy var productPrice = productSubject
        .asObservable()
        .map(\.price)
        .map { [weak self] in
            self?.priceFormatter.format($0)
        }
    
    lazy var productImage = productSubject
        .asObservable()
        .compactMap(\.imagesUrl?.small)
        .compactMap { [weak self] in
            self?.imagesDownloader.image(for: $0)
        }
        .switchLatest()
    
    lazy var isProductSpecialBrand = productSubject
        .asObservable()
        .map(\.isSpecialBrand)
    
    lazy var deselectProduct = CocoaAction { [weak self] in
        self?.productsRepository.selectProduct(nil)
            .map { _ in () } ?? .empty()
    }
    
    // MARK: - Private properties
    
    private let imagesDownloader: ImagesDownloader
    private let priceFormatter: PriceFormatter
    private let productsRepository: ProductsRepository
    private let disposeBag = DisposeBag()
    private let productSubject = ReplaySubject<Product>.create(bufferSize: 1)
    
    // MARK: - Init
    
    init(imagesDownloader: ImagesDownloader,
         priceFormatter: PriceFormatter,
         productsRepository: ProductsRepository) {
        self.imagesDownloader = imagesDownloader
        self.priceFormatter = priceFormatter
        self.productsRepository = productsRepository
        super.init()
        
        bindObservers()
    }
    
    // MARK: - Private methods
    
    private func bindObservers() {
        productsRepository.fetchSelectedProduct()
            .compactMap { $0 }
            .bind(to: productSubject)
            .disposed(by: disposeBag)
    }
}
