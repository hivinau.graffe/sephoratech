// Copyright © Hivinau GRAFFE. All rights reserved.

import Action
import Domain
import RxSwift
import UIKit

final class ProductDetailsCoordinatorAdapter: NSObject, ProductDetailsCoordinator {
    
    // MARK: - Error
    
    private enum ProductDetailsCoordinatorError: Error {
        case startFailed
    }
    
    // MARK: - Private properties
    
    private let window: UIWindowWrapper
    private let productDetailsViewControllerBuilder: any ProductDetailsViewControllerBuilder
    
    private var navigationController: UINavigationController? {
        window.rootViewController as? UINavigationController
    }
    
    private var productDetailsViewController: ProductDetailsViewController? {
        productDetailsViewControllerBuilder.build()
    }
    
    // MARK: - Init
    
    init(window: UIWindowWrapper,
         productDetailsViewControllerBuilder: any ProductDetailsViewControllerBuilder) {
        self.window = window
        self.productDetailsViewControllerBuilder = productDetailsViewControllerBuilder
        super.init()
    }
    
    // MARK: - Coordinator methods
    
    func start() -> Completable {
        return showProductDetailsViewController()
    }
    
    // MARK: - Private methods
    
    private func showProductDetailsViewController() -> Completable {
        guard let productDetailsViewController = productDetailsViewController else {
            return .error(ProductDetailsCoordinatorError.startFailed)
        }
        
        navigationController?.pushViewController(productDetailsViewController, animated: true)
        
        return .empty()
    }
}
