// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Swinject
import SwinjectAutoregistration
import UIKit

struct ProductDetailsAssembly: Assembly {
    
    // MARK: - Assembly methods
    
    func assemble(container: Container) {
        assembleProductDetailsCoordinator(container: container)
        assembleProductDetailsViewModel(container: container)
        assembleProductDetailsViewController(container: container)
        assembleProductDetailsViewControllerBuilder(container: container)
    }
    
    // MARK: - Private methods
    
    private func assembleProductDetailsCoordinator(container: Container) {
        container.autoregister(ProductDetailsCoordinator.self, initializer: ProductDetailsCoordinatorAdapter.init)
            .inObjectScope(.container)
    }
    
    private func assembleProductDetailsViewController(container: Container) {
        container.register(ProductDetailsViewController.self) { resolver in
            let productDetailsViewController = ProductDetailsViewController()
            productDetailsViewController.viewModel = resolver~>
            return productDetailsViewController
        }
        .inObjectScope(.transient)
    }
    
    private func assembleProductDetailsViewControllerBuilder(container: Container) {
        container.register((any ProductDetailsViewControllerBuilder).self) {
            ProductDetailsViewControllerBuilderAdapter(resolver: $0)
        }
        .inObjectScope(.container)
    }
    
    private func assembleProductDetailsViewModel(container: Container) {
        container.autoregister(ProductDetailsViewModel.self, initializer: ProductDetailsViewModelAdapter.init)
            .inObjectScope(.transient)
    }
}
