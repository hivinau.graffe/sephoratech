// Copyright © Hivinau GRAFFE. All rights reserved.

import Swinject
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    // MARK: - Properties
    
    let assembler = Assembler()
    lazy var resolver: Resolver = {
        [
            DomainsAssembly(assembler: assembler),
            ComponentsAssembly(assembler: assembler)
        ]
            .forEach { (assembly: Assembly) in
                assembler.apply(assembly: assembly)
            }
        
        return assembler.resolver
    }()
    
    // MARK: - UIApplicationDelegate methods
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        true
    }
    
    func application(_ application: UIApplication,
                     configurationForConnecting connectingSceneSession: UISceneSession,
                     options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        .init(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
}
