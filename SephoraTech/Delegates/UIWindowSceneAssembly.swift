// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation
import Swinject
import SwinjectAutoregistration
import UIKit

final class UIWindowSceneAssembly: NSObject, Assembly {
    
    // MARK: - Private properties
    
    private let windowScene: UIWindowScene
    
    // MARK: - Init
    
    init(windowScene: UIWindowScene) {
        self.windowScene = windowScene
    }
    
    // MARK: - Assembly methods
    
    func assemble(container: Container) {
        container.register(UIWindowScene.self) { [windowScene] _ in
            return windowScene
        }
        .inObjectScope(.container)
    }
}
