// Copyright © Hivinau GRAFFE. All rights reserved.

import RxSwift
import SwiftUI
import Swinject
import SwinjectAutoregistration
import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    // MARK: - UIWindowSceneDelegate properties
    
    var window: UIWindow?
    
    // MARK: - Private properties
    
    private let disposeBag = DisposeBag()
    private var appDelegate: AppDelegate? {
        UIApplication.shared.delegate as? AppDelegate
    }
    
    private var assembler: Assembler? {
        appDelegate?.assembler
    }
    
    private var resolver: Resolver? {
        appDelegate?.resolver
    }
    
    // MARK: - UIWindowSceneDelegate methods
    
    func scene(_ scene: UIScene,
               willConnectTo session: UISceneSession,
               options connectionOptions: UIScene.ConnectionOptions) {
        guard let resolver = resolver,
              let windowScene = scene as? UIWindowScene else { return }
        
        let windowSceneAssembly = UIWindowSceneAssembly(windowScene: windowScene)
        assembler?.apply(assembly: windowSceneAssembly)
        
        let productsCoordinator = resolver~>(ProductsCoordinator.self)
        self.window = productsCoordinator.window.raw()
        productsCoordinator.start()
            .subscribe()
            .disposed(by: disposeBag)
    }
}
