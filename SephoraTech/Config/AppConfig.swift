// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

struct AppConfig {
    
    enum Remote {
        static let endpoint = AppBundle.value(for: "ENDPOINT") ?? ""
    }
    
    enum App {
        static let displayName = AppBundle.value(for: "PRODUCT_DISPLAY_NAME") ?? ""
    }
    
    private struct AppBundle {
        static var main: Bundle {
            let mainBundle = Bundle.main
            guard mainBundle.bundleURL.pathExtension == "appex" else {
                return mainBundle
            }
            
            let url = mainBundle.bundleURL.deletingLastPathComponent().deletingLastPathComponent()
            return Bundle(url: url) ?? mainBundle
        }
        
        static func value(for key: String) -> String? {
            return AppBundle.main.object(forInfoDictionaryKey: key) as? String
        }
    }
}
