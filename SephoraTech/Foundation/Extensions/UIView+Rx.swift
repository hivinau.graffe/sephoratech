// Copyright © Hivinau GRAFFE. All rights reserved.

import RxSwift
import UIKit

extension Reactive where Base: UIView {
    
    var shadowRadius: Binder<CGFloat> {
        Binder(base) { $0.layer.shadowRadius = $1 }
    }
}
