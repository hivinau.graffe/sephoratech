// Copyright © Hivinau GRAFFE. All rights reserved.

import UIKit

extension UIColor {
    static let backgroundPrimaryColor = UIColor(named: "backgroundPrimaryColor")
    static let backgroundSecondaryColor = UIColor(named: "backgroundSecondaryColor")
    static let backgroundTertiaryColor = UIColor(named: "backgroundTertiaryColor")
    static let secondaryColor = UIColor(named: "secondaryColor")
    static let primaryColor = UIColor(named: "primaryColor")
    static let shadowColor = UIColor(named: "shadowColor")
}
