// Copyright © Hivinau GRAFFE. All rights reserved.

import UIKit

extension UIFont.TextStyle {
    static let headline = UIFont.TextStyle(rawValue: "headline")
    static let title3 = UIFont.TextStyle(rawValue: "title3")
    static let body = UIFont.TextStyle(rawValue: "body")
}
