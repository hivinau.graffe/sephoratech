// Copyright © Hivinau GRAFFE. All rights reserved.

import RxCocoa
import RxSwift
import UIKit

public extension Reactive where Base: UIViewController {
    
    var viewDidAppear: Observable<Bool> {
        let source = methodInvoked(#selector(Base.viewDidAppear(_:)))
            .compactMap { $0.first as? Bool }
        return ControlEvent(events: source).asObservable()
    }
    
    var viewDidDisappear: Observable<Bool> {
        let source = methodInvoked(#selector(Base.viewDidDisappear(_:)))
            .compactMap { $0.first as? Bool }
        return ControlEvent(events: source).asObservable()
    }
    
    var traitCollectionDidChange: Observable<UITraitCollection?> {
        let source = methodInvoked(#selector(Base.traitCollectionDidChange))
            .map { $0.first as? UITraitCollection }
        return ControlEvent(events: source).asObservable()
    }
}
