// Copyright © Hivinau GRAFFE. All rights reserved.

import RxCocoa
import RxSwift
import UIKit

public extension Reactive where Base: UITableViewCell {
    
    var setHighlighted: Observable<Bool> {
        let source = methodInvoked(#selector(Base.setHighlighted(_:animated:)))
            .compactMap { $0.first as? Bool }
        return ControlEvent(events: source).asObservable()
    }
}
