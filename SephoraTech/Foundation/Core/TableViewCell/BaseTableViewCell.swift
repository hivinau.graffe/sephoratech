// Copyright © Hivinau GRAFFE. All rights reserved.

import UIKit

class BaseTableViewCell<ViewModel>: UITableViewCell {
    
    // MARK: - Properties
    
    var viewModel: ViewModel?
}
