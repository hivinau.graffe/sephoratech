// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

// sourcery: AutoMockable
protocol ViewControllerBuilder {
    associatedtype ViewController
    
    func build() -> ViewController?
}
