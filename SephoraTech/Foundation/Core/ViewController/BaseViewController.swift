// Copyright © Hivinau GRAFFE. All rights reserved.

import UIKit

class BaseViewController<ViewModel>: UIViewController {
    
    // MARK: - Properties
    
    var viewModel: ViewModel?
}
