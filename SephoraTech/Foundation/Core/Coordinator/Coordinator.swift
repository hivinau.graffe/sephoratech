// Copyright © Hivinau GRAFFE. All rights reserved.

import RxSwift

// sourcery: AutoMockable
protocol Coordinator: AnyObject {
    func start() -> Completable
}
