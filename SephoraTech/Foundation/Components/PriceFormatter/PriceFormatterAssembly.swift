// Copyright © Hivinau GRAFFE. All rights reserved.

import Data
import Domain
import Swinject
import SwinjectAutoregistration

struct PriceFormatterAssembly: Assembly {
    
    // MARK: - Assembly methods
    
    func assemble(container: Container) {
        container.register(PriceFormatter.self) { resolver in
            var numberFormatter = resolver~>(NumberFormatterWrapper.self)
            numberFormatter.usesGroupingSeparator = true
            numberFormatter.numberStyle = .currency
            numberFormatter.locale = .current
            return PriceFormatterAdapter(numberFormatter: numberFormatter)
        }
        .inObjectScope(.container)
    }
}
