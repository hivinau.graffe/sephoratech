// Copyright © Hivinau GRAFFE. All rights reserved.

import Swinject
import SwinjectAutoregistration

struct ComponentsAssembly: Assembly {
    
    // MARK: - Properties
    
    let assembler: Assembler
    
    // MARK: - Assembly methods
    
    func assemble(container: Container) {
        [
            DataSourcesAssembly(),
            ImagesDownloaderAssembly(),
            PriceFormatterAssembly(),
            RepositoriesAssembly(),
            RequestsAssembly(),
            RoutesAssembly(),
            SessionsAssembly(),
            UseCasesAssembly(),
            WrappersAssembly()
        ]
            .forEach { (assembly: Assembly) in
                assembler.apply(assembly: assembly)
            }
    }
}
