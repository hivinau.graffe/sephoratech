// Copyright © Hivinau GRAFFE. All rights reserved.

import Data
import Domain
import Swinject
import SwinjectAutoregistration

struct DataSourcesAssembly: Assembly {
    
    // MARK: - Assembly methods
    
    func assemble(container: Container) {
        assembleReaderDataSource(container: container)
        assembleReaderWriterDataSource(container: container)
    }
    
    // MARK: - Private methods
    
    private func assembleReaderDataSource(container: Container) {
        container.autoregister(ReaderDataSource.self, initializer: ProductsRemoteDataSourceAdapter.init)
            .inObjectScope(.container)
    }
    
    private func assembleReaderWriterDataSource(container: Container) {
        container.autoregister((ReaderDataSource & WriterDataSource).self, initializer: ProductsLocalDataSourceAdapter.init)
            .inObjectScope(.container)
    }
}
