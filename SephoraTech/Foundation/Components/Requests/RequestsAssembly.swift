// Copyright © Hivinau GRAFFE. All rights reserved.

import Data
import Domain
import Swinject
import SwinjectAutoregistration

struct RequestsAssembly: Assembly {
    
    // MARK: - Assembly methods
    
    func assemble(container: Container) {
        assembleGetProductsRequest(container: container)
    }
    
    // MARK: - Private methods
    
    private func assembleGetProductsRequest(container: Container) {
        container.autoregister(GetProductsRequest.self, initializer: GetProductsRequest.init)
            .inObjectScope(.container)
    }
}
