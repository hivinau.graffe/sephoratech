// Copyright © Hivinau GRAFFE. All rights reserved.

import Data
import Domain
import Swinject
import SwinjectAutoregistration

struct RepositoriesAssembly: Assembly {
    
    // MARK: - Assembly methods
    
    func assemble(container: Container) {
        assembleProductsRepository(container: container)
    }
    
    // MARK: - Private methods
    
    private func assembleProductsRepository(container: Container) {
        container.autoregister(ProductsRepository.self, initializer: ProductsRepositoryAdapter.init)
            .inObjectScope(.container)
    }
}
