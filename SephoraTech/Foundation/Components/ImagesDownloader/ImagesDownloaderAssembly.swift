// Copyright © Hivinau GRAFFE. All rights reserved.

import Data
import Domain
import Swinject
import SwinjectAutoregistration

struct ImagesDownloaderAssembly: Assembly {
    
    // MARK: - Assembly methods
    
    func assemble(container: Container) {
        container.autoregister(ImagesDownloader.self, initializer: ImagesDownloaderAdapter.init)
            .inObjectScope(.container)
    }
}
