// Copyright © Hivinau GRAFFE. All rights reserved.

import AlamofireImage
import Data
import Domain
import Swinject
import SwinjectAutoregistration
import UIKit

struct WrappersAssembly: Assembly {
    
    // MARK: - Assembly methods
    
    func assemble(container: Container) {
        assembleDispatchQueueWrapper(container: container)
        assembleJSONDecoderWrapper(container: container)
        assembleJSONEncoderWrapper(container: container)
        assembleUIWindowWrapper(container: container)
        assembleUIWindowSceneWrapper(container: container)
        assembleURLSessionWrapper(container: container)
        assembleUserDefaultsWrapper(container: container)
        assembleNumberFormatterWrapper(container: container)
        assembleAlamofireImageDownloaderWrapper(container: container)
    }
    
    // MARK: - Private methods
    
    private func assembleDispatchQueueWrapper(container: Container) {
        container.autoregister(DispatchQueueWrapper.self, initializer: DispatchQueueWrapperAdapter.init)
            .inObjectScope(.container)
        
        container.register(Dispatcher.self) { _ in
            return DispatchQueue.main
        }
        .inObjectScope(.container)
    }
    
    private func assembleJSONDecoderWrapper(container: Container) {
        container.autoregister(JSONDecoder.self, initializer: JSONDecoder.init)
            .inObjectScope(.container)
        
        container.autoregister(JSONDecoderWrapper.self, initializer: JSONDecoderWrapperAdapter.init)
            .inObjectScope(.container)
    }
    
    private func assembleJSONEncoderWrapper(container: Container) {
        container.autoregister(JSONEncoder.self, initializer: JSONEncoder.init)
            .inObjectScope(.container)
        
        container.autoregister(JSONEncoderWrapper.self, initializer: JSONEncoderWrapperAdapter.init)
            .inObjectScope(.container)
    }
    
    private func assembleUIWindowWrapper(container: Container) {
        container.register(UIWindowWrapper.self) {
            let windowScene = $0~>(UIWindowSceneWrapper.self)
            let rawWindow = windowScene.window().raw()
            return UIWindowWrapperAdapter(window: rawWindow)
        }
        .inObjectScope(.container)
    }
    
    private func assembleUIWindowSceneWrapper(container: Container) {
        container.autoregister(UIWindowSceneWrapper.self, initializer: UIWindowSceneWrapperAdapter.init)
            .inObjectScope(.container)
    }
    
    private func assembleURLSessionWrapper(container: Container) {
        container.autoregister(URLSessionWrapper.self, initializer: URLSessionWrapperAdapter.init)
            .inObjectScope(.container)
        
        container.register(URLSession.self) { _ in
            let configuration = URLSessionConfiguration.default
            configuration.httpMaximumConnectionsPerHost = 1
            return URLSession(configuration: configuration,
                              delegate: nil,
                              delegateQueue: nil)
        }
        .inObjectScope(.container)
        
        container.autoregister(URLSessionDataTaskWrapperFactory.self, initializer: URLSessionDataTaskWrapperFactory.init)
            .inObjectScope(.container)
    }
    
    private func assembleUserDefaultsWrapper(container: Container) {
        container.register(UserDefaults.self) { _ in
            return .standard
        }
        .inObjectScope(.container)
        
        container.autoregister(UserDefaultsWrapper.self, initializer: UserDefaultsWrapperAdapter.init)
            .inObjectScope(.container)
    }
    
    private func assembleNumberFormatterWrapper(container: Container) {
        container.autoregister(NumberFormatter.self, initializer: NumberFormatter.init)
            .inObjectScope(.container)
        
        container.autoregister(NumberFormatterWrapper.self, initializer: NumberFormatterWrapperAdapter.init)
            .inObjectScope(.container)
    }
    
    private func assembleAlamofireImageDownloaderWrapper(container: Container) {
        container.register(AFImageDownloader.self) { _ in
            return AFImageDownloader()
        }
        .inObjectScope(.container)
        
        container.autoregister(AlamofireImageDownloaderWrapper.self,
                               initializer: AlamofireImageDownloaderWrapperAdapter.init)
        .inObjectScope(.container)
    }
}
