// Copyright © Hivinau GRAFFE. All rights reserved.

import Data
import Domain
import Swinject
import SwinjectAutoregistration

struct SessionsAssembly: Assembly {
    
    // MARK: - Assembly methods
    
    func assemble(container: Container) {
        container.autoregister(Session.self, initializer: SessionAdapter.init)
            .inObjectScope(.container)
    }
}
