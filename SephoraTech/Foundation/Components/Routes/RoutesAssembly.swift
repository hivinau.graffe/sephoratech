// Copyright © Hivinau GRAFFE. All rights reserved.

import Data
import Domain
import Swinject
import SwinjectAutoregistration

struct RoutesAssembly: Assembly {
    
    // MARK: - Assembly methods
    
    func assemble(container: Container) {
        assembleGetProductsRoute(container: container)
    }
    
    // MARK: - Private methods
    
    private func assembleGetProductsRoute(container: Container) {
        container.register(GetProductsRoute.self) { _ in
            return GetProductsRoute(endpoint: AppConfig.Remote.endpoint)
        }
        .inObjectScope(.container)
    }
}
