// Copyright © Hivinau GRAFFE. All rights reserved.

import Data
import Domain
import Swinject
import SwinjectAutoregistration

struct UseCasesAssembly: Assembly {
    
    // MARK: - Assembly methods
    
    func assemble(container: Container) {
        container.autoregister(FetchProductsUseCase.self, initializer: FetchProductsUseCaseAdapter.init)
            .inObjectScope(.container)
    }
}
