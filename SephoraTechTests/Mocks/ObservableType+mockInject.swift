// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation
import RxCocoa
import RxSwift

private var xoSubjectAssociationKey: UInt8 = 0
private var xoSpyAssociationKey: UInt8 = 1

extension ObservableType {
    
    static func mockInject() -> Observable<Element> {
        let subject = ReplaySubject<Element>.create(bufferSize: 1)
        let observable = subject.asObservable()
        observable.storeSubject(newValue: subject)
        return observable
    }
    
    func storeSubject(newValue: ReplaySubject<Element>) {
        objc_setAssociatedObject(self, &xoSubjectAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
    }
    
    var underlyingObserver: ReplaySubject<Element> {
        get {
            guard let result = objc_getAssociatedObject(self, &xoSubjectAssociationKey) as? ReplaySubject<Element> else {
                assertionFailure("Observable must be created with Observable<T>.mockInject() to make magic happen")
                return ReplaySubject<Element>.create(bufferSize: 1)
            }
            return result
        }
    }
}
