// Copyright © Hivinau GRAFFE. All rights reserved.

import SwiftyMocky
import XCTest

public extension Mock {
    
    func verifyEventually(
        test: XCTestCase,
        _ method: Verify,
        count: Count = Count.moreOrEqual(to: 1),
        file: StaticString = #file,
        line: UInt = #line,
        timeout: DispatchTimeInterval = .seconds(1)
    ) {
        let expectation = XCTestExpectation(description: "VerifyEventually \(method.self)")
        
        let timer = DispatchSource.makeTimerSource(queue: DispatchQueue.global())
        timer.schedule(deadline: .now() + timeout)
        timer.setEventHandler {
            expectation.fulfill()
        }
        timer.resume()
        
        test.wait(for: [expectation], timeout: 5)
        
        self.verify(method, count: count, file: file, line: line)
    }
}
