// Copyright © Hivinau GRAFFE. All rights reserved.

import Action
import Foundation
import RxSwift

class SpyCocoaAction<T> {
    
    // MARK: - Properties
    
    private(set) var lastValue: T?
    private(set) var callsCount = 0
    
    // MARK: - Functions
    
    func generateAction() -> Action<T, Void>  {
        return Action<T, Void> { value in
            self.lastValue = value
            self.callsCount += 1
            return Observable<Void>.empty()
        }
    }
}
