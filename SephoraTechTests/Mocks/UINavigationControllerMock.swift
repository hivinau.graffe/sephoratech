// Copyright © Hivinau GRAFFE. All rights reserved.

import UIKit

final class UINavigationControllerMock: UINavigationController {
    
    var pushViewControllerCallsCount = 0
    var pushViewControllerCalled: Bool {
        pushViewControllerCallsCount > 0
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        pushViewControllerCallsCount += 1
    }
}
