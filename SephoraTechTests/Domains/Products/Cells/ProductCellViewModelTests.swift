// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Nimble
import RxSwift
import UIKit
import XCTest
@testable import SephoraTech

final class ProductCellViewModelTests: XCTestCase {
    
    // MARK: - Private properties
    
    private var tested: ProductCellViewModelAdapter!
    private let priceFormatter = PriceFormatterMock()
    private lazy var imagesDownloader: ImagesDownloaderMock = {
        let imagesDownloader = ImagesDownloaderMock()
        imagesDownloader.given(.image(for: .any, willReturn: Observable<UIImage?>.mockInject()))
        return imagesDownloader
    }()
    
    
    // MARK: - Lifecycle
    
    override func setUp() {
        tested = ProductCellViewModelAdapter(imagesDownloader: imagesDownloader,
                                             priceFormatter: priceFormatter)
        super.setUp()
    }
    
    // MARK: - Tests
    
//    func testProductNameSubscribe_shouldNeverOnNextValidProductName_whenProductObservableOnNextProductWithInvalidName() {
//        var productName: String?
//        _ = tested.productName
//            .subscribe(onNext: {
//                productName = $0
//            })
//        
//        _ = tested.product.execute(createProduct())
//        
//        expect(productName).toEventually(beNil())
//    }
    
//    func testProductNameSubscribe_shouldOnNextValidProductName_whenProductObservableOnNextProductWithValidName() {
//        let expectedProductName = "test"
//        let product = createProduct(name: expectedProductName)
//        productObservable.underlyingObserver.on(.next(product))
//        var productName: String?
//
//        _ = tested.productName
//            .subscribe(onNext: {
//                productName = $0
//            })
//
//        expect(productName).toEventually(equal(expectedProductName))
//    }
    
//    func testProductDescriptionSubscribe_shouldNeverOnNextValidProductDescriptionFr_whenProductObservableOnNextProductWithInvalidDescriptionFr() throws {
//        tested.product.execute(createProduct())
//
//        let productDescriptionFr = try tested.productDescription.toBlocking().first()
//
//        expect(productDescriptionFr).toEventually(beNil())
//    }
    
//    func testProductDescriptionSubscribe_shouldOnNextValidProductDescriptionFr_whenProductObservableOnNextProductWithValidDescriptionFr() {
//        let expectedProductDescriptionFr = "test"
//        let product = createProduct(descriptionFr: expectedProductDescriptionFr)
//        productObservable.underlyingObserver.on(.next(product))
//        var productDescriptionFr: String?
//
//        _ = tested.productDescription
//            .subscribe(onNext: {
//                productDescriptionFr = $0
//            })
//
//        expect(productDescriptionFr).toEventually(equal(expectedProductDescriptionFr))
//    }
    
//    func testIsProductSpecialBrandSubscribe_shouldNeverOnNextValidProductIsSpecialBrand_whenProductObservableOnNextProductWithInvalidIsSpecialBrand() throws {
//        tested.product.execute(createProduct())
//
//        let isSpecialBrand = try tested.isProductSpecialBrand.toBlocking().first()
//
//        expect(isSpecialBrand).toEventually(beFalse())
//    }
    
//    func testIsProductSpecialBrandBrandSubscribe_shouldOnNextValidProductIsSpecialBrand_whenProductObservableOnNextProductWithValidIsSpecialBrand() {
//        let expectedProductIsSpecialBrand = true
//        let product = createProduct(isSpecialBrand: expectedProductIsSpecialBrand)
//        productObservable.underlyingObserver.on(.next(product))
//        var isSpecialBrand: Bool?
//
//        _ = tested.isProductSpecialBrand
//            .subscribe(onNext: {
//                isSpecialBrand = $0
//            })
//
//        expect(isSpecialBrand).toEventually(equal(expectedProductIsSpecialBrand))
//    }
    
//    func testProductPriceSubscribe_shouldNeverOnNextValidProductPrice_whenProductObservableOnNextProductWithInvalidPrice() throws {
//        tested.product.execute(createProduct())
//
//        let price = try tested.productPrice.toBlocking().first()
//
//        expect(price).toEventually(beNil())
//    }
    
//    func testProductPriceSubscribe_shouldNeverOnNextValidProductPrice_whenProductObservableOnNextProductWithValidPriceAndPriceFormatterFormatReturnsNil() throws {
//        tested.product.execute(createProduct())
//
//        priceFormatter.given(.format(.any, willReturn: nil))
//
//        let price = try tested.productPrice.toBlocking().first()
//
//        expect(price).toEventually(beNil())
//    }
    
//    func testProductPriceSubscribe_shouldOnNextValidProductPrice_whenProductObservableOnNextProductWithValidPriceAndPriceFormatterFormatReturnsValidPrice() {
//        let expectedProductPrice = "0"
//        let product = createProduct(price: .zero)
//        productObservable.underlyingObserver.on(.next(product))
//        priceFormatter.given(.format(.any, willReturn: expectedProductPrice))
//        var price: String?
//
//        _ = tested.productPrice
//            .subscribe(onNext: {
//                price = $0
//            })
//
//        expect(price).toEventually(equal(expectedProductPrice))
//    }
    
//    func testProductImageSubscribe_shouldNeverOnNextValidProductImage_whenProductObservableOnNextProductWithInvalidImagesUrl() throws {
//        tested.product.execute(createProduct())
//
//        let image = try tested.productImage.toBlocking().first()
//
//        expect(image).toEventually(beNil())
//    }
    
//    func testProductImageSubscribe_shouldNeverOnNextValidProductImage_whenProductObservableOnNextProductWithValidImagesUrlAndNilSmall() throws {
//        let imagesUrl = ImagesUrl(small: nil, large: nil)
//        tested.product.execute(createProduct(imagesUrl: imagesUrl))
//
//        let image = try tested.productImage.toBlocking().first()
//
//        expect(image).toEventually(beNil())
//    }
    
//    func testProductImageSubscribe_shouldNeverOnNextValidProductImage_whenProductObservableOnNextProductWithValidImageAndImageDownloaderImageReturnsEmptyObservable() throws {
//        let imagesUrl = ImagesUrl(small: "test", large: nil)
//        tested.product.execute(createProduct(imagesUrl: imagesUrl))
//
//        imagesDownloader.given(.image(for: .any, willReturn: .empty()))
//
//        let image = try tested.productImage.toBlocking().first()
//
//        expect(image).toEventually(beNil())
//    }
    
//    func testProductImageSubscribe_shouldOnNextValidProductImage_whenProductObservableOnNextProductWithValidImageAndImageDownloaderImageReturnsObservableOfValidImage() {
//        let imagesUrl = ImagesUrl(small: "test")
//        let expectedProductImage = UIImage()
//        let product = createProduct(imagesUrl: imagesUrl)
//        productObservable.underlyingObserver.on(.next(product))
//        imageDownloader.given(.image(for: .any, willReturn: .just(expectedProductImage)))
//        var image: UIImage?
//        
//        _ = tested.productImage
//            .subscribe(onNext: {
//                image = $0
//            })
//        
//        expect(image).toEventually(equal(expectedProductImage))
//    }
    
    // MARK: - Private methods
    
    private func createProduct(id: Int = .zero,
                               name: String? = nil,
                               descriptionFr: String? = nil,
                               price: Double = .zero,
                               isProductSet: Bool = false,
                               isSpecialBrand: Bool = false,
                               imagesUrl: ImagesUrl? = nil) -> Product {
        return .init(id: id,
                     name: name,
                     descriptionFr: descriptionFr,
                     price: price,
                     isProductSet: isProductSet,
                     isSpecialBrand: isSpecialBrand,
                     imagesUrl: imagesUrl)
    }
}
