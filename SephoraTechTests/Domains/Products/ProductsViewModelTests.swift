// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Nimble
import RxBlocking
import RxSwift
import XCTest
@testable import SephoraTech

final class ProductsViewModelTests: XCTestCase {
    
    // MARK: - Private properties
    
    private var tested: ProductsViewModelAdapter!
    private let dataSource = ProductsViewModelDataSourceMock()
    private let appName = "test"
    private lazy var productsRepository: ProductsRepositoryMock = {
        let productsRepository = ProductsRepositoryMock()
        productsRepository.given(.fetchProducts(willReturn: Observable<[Product]>.mockInject()))
        return productsRepository
    }()
    
    // MARK: - Lifecycle
    
    override func setUp() {
        tested = ProductsViewModelAdapter(appName: appName,
                                          productsRepository: productsRepository)
        tested.dataSource = dataSource
        super.setUp()
    }
    
    // MARK: - Tests
    
    func testTitle_shouldBeSet_whenAppNameSet() throws {
        let title = try tested.title.toBlocking().first()
        
        expect(title).toEventually(equal(self.appName))
    }
    
    func testCellTypes_shouldBeSet_whenProductsRepositoryFetchProductsCalled() {
        let products = [
            Product(id: .zero,
                    name: nil,
                    descriptionFr: nil,
                    price: .zero,
                    isProductSet: false,
                    isSpecialBrand: false,
                    imagesUrl: nil)
        ]
        let observable = productsRepository.fetchProducts()
        observable.underlyingObserver.on(.next(products))
        let cellTypes = tested.cellTypes.toBlocking()
        
        expect(cellTypes).toNotEventually(beNil())
    }
}
