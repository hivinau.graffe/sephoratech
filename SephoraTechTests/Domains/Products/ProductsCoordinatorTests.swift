// Copyright © Hivinau GRAFFE. All rights reserved.

import Nimble
import RxBlocking
import RxSwift
import XCTest
@testable import SephoraTech

final class ProductsCoordinatorTests: XCTestCase {
    
    // MARK: - Private properties
    
    private var tested: ProductsCoordinatorAdapter!
    private var disposeBag: DisposeBag!
    private let dataSource = ProductsCoordinatorDataSourceMock()
    private let window = UIWindowWrapperMock()
    
    // MARK: - Lifecycle
    
    override func setUp() {
        tested = ProductsCoordinatorAdapter(window: window)
        tested.dataSource = dataSource
        disposeBag = DisposeBag()
        super.setUp()
    }
    
    // MARK: - Tests
    
    func testWindowRootViewController_shouldBeSet_whenStartCalledAndDataSourceProductsViewControllerReturnsValidController() {
        dataSource.given(.productsViewController(.any, willReturn: ProductsViewController()))
        
        _ = tested.start().toBlocking().materialize()
        
        window.verify(.rootViewController(set: .any))
    }
    
    func testWindowRootViewController_shouldNeverBeSet_whenStartCalledAndDataSourceProductsViewControllerReturnsNil() {
        dataSource.given(.productsViewController(.any, willReturn: nil))
        
        _ = tested.start().toBlocking().materialize()
        
        window.verify(.rootViewController(set: .any), count: .never)
    }
    
    func testWindowMakeKeyAndVisible_shouldBeCalled_whenStartCalledAndDataSourceProductsViewControllerReturnsValidController() {
        let productsViewController = ProductsViewController()
        dataSource.given(.productsViewController(.any, willReturn: productsViewController))
        
        _ = tested.start().toBlocking().materialize()
        
        window.verify(.makeKeyAndVisible())
    }
    
    func testWindowMakeKeyAndVisible_shouldNeverBeCalled_whenStartCalledAndDataSourceProductsViewControllerReturnsNil() {
        dataSource.given(.productsViewController(.any, willReturn: nil))
        
        _ = tested.start().toBlocking().materialize()
        
        window.verify(.makeKeyAndVisible(), count: .never)
    }
}
