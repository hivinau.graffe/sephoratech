// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Nimble
import RxCocoa
import RxSwift
import XCTest
@testable import SephoraTech

final class ProductsViewControllerTests: XCTestCase {
    
    // MARK: - Private properties
    
    private var tested: ProductsViewController!
    private lazy var viewModel: ProductsViewModelMock = {
        let viewModel = ProductsViewModelMock()
        viewModel.given(.cellTypes(getter: Observable<[CellType]>.mockInject()))
        viewModel.given(.title(getter: Observable<String?>.mockInject()))
        viewModel.given(.selectedIndexPath(getter: SpyCocoaAction<IndexPath>().generateAction()))
        viewModel.given(.selectedProduct(getter: SpyCocoaAction<Product>().generateAction()))
        return viewModel
    }()
    
    // MARK: - Lifecycle
    
    override func setUp() {
        tested = ProductsViewController()
        tested.viewModel = viewModel
        super.setUp()
    }
    
    // MARK: - Tests
    
    func testViewBackgroundColor_shouldEqualBackgroundPrimaryColor_whenViewDidSet() {
        _ = tested.view
        
        expect(self.tested.view.backgroundColor).to(equal(.backgroundPrimaryColor))
    }
    
    func testTableViewBackgroundColor_shouldEqualClear_whenTableViewDidSet() {
        _ = tested.view
        
        expect(self.tested.tableView.backgroundColor).to(equal(.clear))
    }
    
    func testTableViewSeparatorStyle_shouldEqualNone_whenTableViewDidSet() {
        _ = tested.view
        
        expect(self.tested.tableView.separatorStyle).to(equal(UITableViewCell.SeparatorStyle.none))
    }
    
    func testTableViewRowHeight_shouldEqualUITableViewAutomaticDimension_whenTableViewDidSet() {
        _ = tested.view
        
        expect(self.tested.tableView.rowHeight).to(equal(UITableView.automaticDimension))
    }
    
    func testTitle_shouldBeSet_whenViewModelTitleOnNextTitle() {
        _ = tested.view
        let title = "title"
        
        viewModel.title.underlyingObserver.on(.next(title))
        
        expect(self.tested.title).toEventually(equal(title))
    }
    
    func testCellViewModel_shouldBeSet_whenViewModelCellTypesOnNextCellTypes() {
        _ = tested.view
        tested.viewDidAppear(false)
        tested.traitCollectionDidChange(.current)
        let productCellViewModel = ProductCellViewModelMock()
        productCellViewModel.given(.productName(getter: Observable<String?>.mockInject()))
        productCellViewModel.given(.productImage(getter: Observable<UIImage?>.mockInject()))
        productCellViewModel.given(.productPrice(getter: Observable<String?>.mockInject()))
        productCellViewModel.given(.isProductSpecialBrand(getter: Observable<Bool>.mockInject()))
        productCellViewModel.given(.productDescription(getter: Observable<String?>.mockInject()))
        let cellTypes = [CellType.product(productCellViewModel)]
        
        viewModel.cellTypes.underlyingObserver.on(.next(cellTypes))
        
        let cell = tested.tableView.cellForRow(at: .init(row: 0, section: 0)) as! ProductCell
        expect(cell.viewModel).toEventually(beAnInstanceOf(ProductCellViewModelMock.self))
    }
}
