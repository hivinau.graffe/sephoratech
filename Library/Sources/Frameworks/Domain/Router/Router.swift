// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

// sourcery: AutoMockable
public protocol Router: AnyObject {
    func route(to route: Route) throws -> URLRequest
}
