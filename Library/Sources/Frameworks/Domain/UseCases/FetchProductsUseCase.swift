import RxSwift

// sourcery: AutoMockable
public protocol FetchProductsUseCase {
    
    func start() -> Observable<[Product]>
}
