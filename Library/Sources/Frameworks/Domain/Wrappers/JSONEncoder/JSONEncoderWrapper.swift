// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

// sourcery: AutoMockable
public protocol JSONEncoderWrapper {
    func encode<T: Encodable>(_ encodable: T) throws -> Data
}
