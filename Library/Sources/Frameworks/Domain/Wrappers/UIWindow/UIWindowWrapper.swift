// Copyright © Hivinau GRAFFE. All rights reserved.

import UIKit

// sourcery: AutoMockable
public protocol UIWindowWrapper {
    var rootViewController: UIViewController? { get set }
    
    func makeKeyAndVisible()
    func raw() -> UIWindow
}
