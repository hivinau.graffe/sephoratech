// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

// sourcery: AutoMockable
public protocol URLSessionDataTaskWrapper {
    func cancel()
    func resume()
    func raw() -> URLSessionDataTask
}
