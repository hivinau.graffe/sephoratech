// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

// sourcery: AutoMockable
public protocol NumberFormatterWrapper {
    var usesGroupingSeparator: Bool { get set }
    var numberStyle: NumberFormatter.Style { get set }
    var locale: Locale { get set }
    
    func string(from value: Double) -> String?
}
