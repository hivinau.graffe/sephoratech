// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

// sourcery: AutoMockable
public protocol JSONDecoderWrapper {
    func decode<T: Decodable>(_ type: T.Type, from data: Data) throws -> T
}
