import Foundation

// sourcery: AutoMockable
public protocol Dispatcher {
    
    @preconcurrency
    func async(
        group: DispatchGroup?,
        qos: DispatchQoS,
        flags: DispatchWorkItemFlags,
        execute work: @escaping @Sendable @convention(block) () -> Void
    )
    
    var asDispatchQueue: DispatchQueue { get }
}

extension DispatchQueue: Dispatcher {
    
    public var asDispatchQueue: DispatchQueue { self }
}
