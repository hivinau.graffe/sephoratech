// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

// sourcery: AutoMockable
public protocol DispatchQueueWrapper {
    func async(execute work: @escaping () -> Void)
    func raw() -> DispatchQueue
}
