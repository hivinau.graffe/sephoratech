// Copyright © Hivinau GRAFFE. All rights reserved.

import UIKit

// sourcery: AutoMockable
public protocol UIWindowSceneWrapper {
    func window() -> UIWindowWrapper
}
