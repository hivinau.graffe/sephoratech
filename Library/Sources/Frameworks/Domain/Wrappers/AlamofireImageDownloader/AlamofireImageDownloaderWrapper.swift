// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation
import UIKit

// sourcery: AutoMockable
public protocol AlamofireImageDownloaderWrapper {
    func downloadImage(_ urlRequest: URLRequest, completion: @escaping (UIImage?) -> Void)
}
