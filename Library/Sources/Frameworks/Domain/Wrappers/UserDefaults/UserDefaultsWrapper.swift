// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

// sourcery: AutoMockable
public protocol UserDefaultsWrapper {
    func value(forKey key: String) -> Any?
    func setValue(_ value: Any?, forKey key: String)
}
