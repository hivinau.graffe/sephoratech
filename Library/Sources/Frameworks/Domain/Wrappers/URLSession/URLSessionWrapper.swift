// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

// sourcery: AutoMockable
public protocol URLSessionWrapper {
    func dataTask(with request: Request,
                  completionHandler: @escaping @Sendable (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTaskWrapper?
}
