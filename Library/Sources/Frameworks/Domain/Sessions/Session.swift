// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation
import RxSwift

// sourcery: AutoMockable
public protocol Session: AnyObject {
    func sendRequest<T: Decodable>(_ request: Request) -> Observable<T>
}
