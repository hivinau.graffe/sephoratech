// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

public enum SessionError: Error {
    case serverAddressRequired
    case cannotCreateUrl
    case receiveHttpErrorCode(Int)
    case cannotDecodeObject
    case unknown
}
