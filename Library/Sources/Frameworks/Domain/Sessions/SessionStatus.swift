// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

public enum SessionStatus: Int {
    case OK = 200
    case SERVER_ERROR = 500
}
