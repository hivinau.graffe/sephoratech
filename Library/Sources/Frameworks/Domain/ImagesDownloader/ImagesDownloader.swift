// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation
import RxSwift
import UIKit

// sourcery: AutoMockable
public protocol ImagesDownloader {
    func image(for urlString: String) -> Observable<UIImage?>
}
