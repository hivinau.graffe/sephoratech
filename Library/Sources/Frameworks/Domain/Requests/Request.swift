// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

// sourcery: AutoMockable
public protocol Request: AnyObject {
    func raw() throws -> URLRequest
}
