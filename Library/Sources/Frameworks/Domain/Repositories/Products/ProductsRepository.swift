// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation
import RxSwift

// sourcery: AutoMockable
public protocol ProductsRepository {
    func fetchProducts() -> Observable<[Product]>
    func fetchSelectedProduct() -> Observable<Product?>
    func selectProduct(_ product: Product?) -> Observable<Product?>
}
