// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

// sourcery: AutoMockable
public protocol PriceFormatter {
    func format(_ value: Double) -> String?
}
