// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

public protocol ImagesUrlConvertible {
    func toImagesUrl() -> ImagesUrl
}

public struct ImagesUrl {
    public let small: String?
    public let large: String?
    
    public init(small: String?,
                large: String?) {
        self.small = small
        self.large = large
    }
}
