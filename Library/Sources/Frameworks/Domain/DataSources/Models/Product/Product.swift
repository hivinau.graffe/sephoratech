// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

public protocol ProductConvertible {
    func toProduct() -> Product
}

public struct Product: Equatable {
    public let id: Int
    public let name: String?
    public let descriptionFr: String?
    public let price: Double
    public let isProductSet: Bool
    public let isSpecialBrand: Bool
    public let imagesUrl: ImagesUrl?
    
    public init(id: Int,
                name: String?,
                descriptionFr: String?,
                price: Double,
                isProductSet: Bool,
                isSpecialBrand: Bool,
                imagesUrl: ImagesUrl?) {
        self.id = id
        self.name = name
        self.descriptionFr = descriptionFr
        self.price = price
        self.isProductSet = isProductSet
        self.isSpecialBrand = isSpecialBrand
        self.imagesUrl = imagesUrl
    }
}

// MARK: - Equatable

extension Product {
    
    public static func == (lhs: Product, rhs: Product) -> Bool {
        return lhs.id == rhs.id
    }
}
