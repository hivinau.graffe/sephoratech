// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation
import RxSwift

// sourcery: AutoMockable
public protocol WriterDataSource {
    func replaceProducts(_ products: [Product]) -> Observable<[Product]>
    func selectProduct(_ product: Product?) -> Observable<Product?>
}
