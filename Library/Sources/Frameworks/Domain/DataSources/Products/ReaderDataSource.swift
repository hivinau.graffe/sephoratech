// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation
import RxSwift

// sourcery: AutoMockable
public protocol ReaderDataSource {
    func fetchProducts() -> Observable<[Product]>
    func fetchSelectedProduct() -> Observable<Product?>
}
