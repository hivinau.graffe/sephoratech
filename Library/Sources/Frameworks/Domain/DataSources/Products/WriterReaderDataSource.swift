// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

// sourcery: AutoMockable
public protocol WriterReaderDataSource: WriterDataSource, ReaderDataSource {}
