// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

public enum HttpMethod: String {
    case get = "GET"
}
