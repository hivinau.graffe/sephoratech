// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

// sourcery: AutoMockable
public protocol Route {
    var method: HttpMethod { get }
    
    func url() -> URL?
}
