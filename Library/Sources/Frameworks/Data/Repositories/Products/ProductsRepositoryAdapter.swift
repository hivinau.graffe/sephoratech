// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Foundation
import RxSwift

public final class ProductsRepositoryAdapter: NSObject, ProductsRepository {
    
    // MARK: - Private properties
    
    private let remoteDataSource: ReaderDataSource
    private let localDataSource: ReaderDataSource & WriterDataSource
    
    // MARK: - Init
    
    public init(remoteDataSource: ReaderDataSource,
         localDataSource: ReaderDataSource & WriterDataSource) {
        self.remoteDataSource = remoteDataSource
        self.localDataSource = localDataSource
        super.init()
    }
    
    // MARK: - ProductsRepository methods
    
    public func fetchProducts() -> Observable<[Product]> {
        let localProductsObservable = localDataSource.fetchProducts()
            .filter { $0.count > 0 }
        
        let remoteProductsObservable = remoteDataSource.fetchProducts()
            .flatMap { [weak self] products in
                self?.localDataSource.replaceProducts(products) ?? .empty()
            }
        
        return .merge(localProductsObservable, remoteProductsObservable)
            .distinctUntilChanged()
    }
    
    public func fetchSelectedProduct() -> Observable<Product?> {
        return localDataSource.fetchSelectedProduct()
    }
    
    public func selectProduct(_ product: Product?) -> Observable<Product?> {
        return localDataSource.selectProduct(product)
    }
}
