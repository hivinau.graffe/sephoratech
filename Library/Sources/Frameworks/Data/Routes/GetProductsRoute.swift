// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Foundation

public struct GetProductsRoute: Route {
    
    // MARK: - Constants
    
    private enum Constants {
        static let routeTemplate = "%@/items.json"
    }
    
    // MARK: - Route properties
    
    public let method = HttpMethod.get
    
    // MARK: - Properties
    
    private let endpoint: String
    
    public init(endpoint: String) {
        self.endpoint = endpoint
    }
    
    // MARK: - Route methods
    
    public func url() -> URL? {
        let urlString = String(format: Constants.routeTemplate, endpoint)
        return URL(string: urlString)
    }
}
