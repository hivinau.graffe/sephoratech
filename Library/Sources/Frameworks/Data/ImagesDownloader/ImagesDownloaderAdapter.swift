// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Foundation
import RxSwift
import UIKit

public final class ImagesDownloaderAdapter: NSObject, ImagesDownloader {
    
    // MARK: - Private properties
    
    private let imageDownloader: AlamofireImageDownloaderWrapper
    
    // MARK: - Init
    
    public init(imageDownloader: AlamofireImageDownloaderWrapper) {
        self.imageDownloader = imageDownloader
    }
    
    // MARK: - ImageDownloader methods
    
    public func image(for urlString: String) -> Observable<UIImage?> {
        return .create { [weak self] observer in
            guard let url = URL(string: urlString) else {
                observer.on(.completed)
                return Disposables.create()
            }
            
            let request = URLRequest(url: url)
            self?.imageDownloader.downloadImage(request,
                                                completion: { [observer] image in
                observer.on(.next(image))
            })
            return Disposables.create()
        }
    }
}
