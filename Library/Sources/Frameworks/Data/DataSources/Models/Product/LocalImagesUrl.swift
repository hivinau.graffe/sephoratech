// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Foundation

public final class LocalImagesUrl: Codable {
    
    // MARK: - CodingKeys
    
    private enum CodingKeys: String, CodingKey {
        case small
        case large
    }
    
    // MARK: - Properties
    
    public let small: String?
    public let large: String?
    
    // MARK: - Init
    
    public required init(small: String? = nil,
                         large: String? = nil) {
        self.small = small
        self.large = large
    }
    
    public convenience init(imagesUrl: ImagesUrl?) {
        self.init(small: imagesUrl?.small,
                  large: imagesUrl?.large)
    }
    
    // MARK: - Decodable methods
    
    public convenience init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let small = try container.decodeIfPresent(String.self, forKey: .small)
        let large = try container.decodeIfPresent(String.self, forKey: .large)
        self.init(small: small, large: large)
    }
    
    // MARK: - Encodable methods
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(small, forKey: .small)
        try container.encodeIfPresent(large, forKey: .large)
    }
}

// MARK: - ProductConvertible

extension LocalImagesUrl: ImagesUrlConvertible {
    
    public func toImagesUrl() -> ImagesUrl {
        return .init(small: small, large: large)
    }
}
