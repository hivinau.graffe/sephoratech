// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Foundation

public final class RemoteImagesUrl: Decodable {
    
    // MARK: - CodingKeys
    
    private enum CodingKeys: String, CodingKey {
        case small
        case large
    }
    
    // MARK: - Properties
    
    public let small: String?
    public let large: String?
    
    // MARK: - Init
    
    public required init(small: String? = nil,
                         large: String? = nil) {
        self.small = small
        self.large = large
    }
    
    // MARK: - Decodable methods
    
    public convenience init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let small = try container.decodeIfPresent(String.self, forKey: .small)
        let large = try container.decodeIfPresent(String.self, forKey: .large)
        self.init(small: small, large: large)
    }
}

// MARK: - ProductConvertible

extension RemoteImagesUrl: ImagesUrlConvertible {
    
    public func toImagesUrl() -> ImagesUrl {
        return .init(small: small, large: large)
    }
}
