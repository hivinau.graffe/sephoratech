// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Foundation

public final class LocalProduct: Codable {
    
    // MARK: - CodingKeys
    
    private enum CodingKeys: String, CodingKey {
        case id = "product_id"
        case name = "product_name"
        case descriptionFr = "description"
        case price = "price"
        case isProductSet = "is_productSet"
        case isSpecialBrand = "is_special_brand"
        case imagesUrl = "images_url"
    }
    
    // MARK: - Properties
    
    public let id: Int
    public let name: String?
    public let descriptionFr: String?
    public let price: Double
    public let isProductSet: Bool
    public let isSpecialBrand: Bool
    public let imagesUrl: LocalImagesUrl?
    
    // MARK: - Init
    
    public required init(id: Int,
                         name: String?,
                         descriptionFr: String?,
                         price: Double,
                         isProductSet: Bool,
                         isSpecialBrand: Bool,
                         imagesUrl: LocalImagesUrl?) {
        self.id = id
        self.name = name
        self.descriptionFr = descriptionFr
        self.price = price
        self.isProductSet = isProductSet
        self.isSpecialBrand = isSpecialBrand
        self.imagesUrl = imagesUrl
    }
    
    public convenience init(product: Product) {
        self.init(id: product.id,
                  name: product.name,
                  descriptionFr: product.descriptionFr,
                  price: product.price,
                  isProductSet: product.isProductSet,
                  isSpecialBrand: product.isSpecialBrand,
                  imagesUrl: .init(imagesUrl: product.imagesUrl))
    }
    
    // MARK: - Decodable methods
    
    public convenience init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let id = try container.decode(Int.self, forKey: .id)
        let name = try container.decodeIfPresent(String.self, forKey: .name)
        let descriptionFr = try container.decodeIfPresent(String.self, forKey: .descriptionFr)
        let price = try container.decode(Double.self, forKey: .price)
        let isProductSet = try container.decode(Bool.self, forKey: .isProductSet)
        let isSpecialBrand = try container.decode(Bool.self, forKey: .isSpecialBrand)
        let imagesUrl = try container.decodeIfPresent(LocalImagesUrl.self, forKey: .imagesUrl)
        self.init(id: id,
                  name: name,
                  descriptionFr: descriptionFr,
                  price: price,
                  isProductSet: isProductSet,
                  isSpecialBrand: isSpecialBrand,
                  imagesUrl: imagesUrl)
    }
    
    // MARK: - Encodable methods
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encodeIfPresent(name, forKey: .name)
        try container.encodeIfPresent(descriptionFr, forKey: .descriptionFr)
        try container.encode(price, forKey: .price)
        try container.encode(isProductSet, forKey: .isProductSet)
        try container.encode(isSpecialBrand, forKey: .isSpecialBrand)
        try container.encodeIfPresent(imagesUrl, forKey: .imagesUrl)
    }
}

// MARK: - ProductConvertible

extension LocalProduct: ProductConvertible {
    
    public func toProduct() -> Product {
        return .init(id: id,
                     name: name,
                     descriptionFr: descriptionFr,
                     price: price,
                     isProductSet: isProductSet,
                     isSpecialBrand: isSpecialBrand,
                     imagesUrl: imagesUrl?.toImagesUrl())
    }
}
