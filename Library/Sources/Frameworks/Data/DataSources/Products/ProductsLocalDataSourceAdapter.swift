// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Foundation
import RxSwift

public final class ProductsLocalDataSourceAdapter: NSObject, ReaderDataSource, WriterDataSource {
    
    // MARK: - Constants
    
    private enum Constants {
        static let productsKey = "productsKey"
        static let selectedProductKey = "selectedProductKey"
    }
    
    // MARK: - Private properties
    
    private let jsonDecoder: JSONDecoderWrapper
    private let jsonEncoder: JSONEncoderWrapper
    private let userDefaults: UserDefaultsWrapper
    
    // MARK: - Init
    
    public init(jsonDecoder: JSONDecoderWrapper,
                jsonEncoder: JSONEncoderWrapper,
                userDefaults: UserDefaultsWrapper) {
        self.jsonDecoder = jsonDecoder
        self.jsonEncoder = jsonEncoder
        self.userDefaults = userDefaults
        super.init()
    }
    
    // MARK: - ReaderDataSource methods
    
    public func fetchProducts() -> Observable<[Product]> {
        return .deferred {
            .create { [weak self] observer in
                guard let data = self?.userDefaults.value(forKey: Constants.productsKey) as? Data else {
                    observer.on(.next([]))
                    observer.on(.completed)
                    return Disposables.create()
                }
                
                do {
                    let localProducts = try self?.jsonDecoder.decode([LocalProduct].self, from: data)
                    guard let localProducts = localProducts else {
                        observer.on(.next([]))
                        observer.on(.completed)
                        return Disposables.create()
                    }
                    
                    let products = localProducts.map { $0.toProduct() }
                    
                    observer.on(.next(products))
                    observer.on(.completed)
                } catch {
                    observer.on(.error(error))
                }
                
                return Disposables.create()
            }
        }
    }
    
    public func fetchSelectedProduct() -> Observable<Product?> {
        return .deferred {
            .create { [weak self] observer in
                guard let data = self?.userDefaults.value(forKey: Constants.selectedProductKey) as? Data else {
                    observer.on(.next(nil))
                    observer.on(.completed)
                    return Disposables.create()
                }
                
                do {
                    let localProduct = try self?.jsonDecoder.decode(LocalProduct.self, from: data)
                    guard let localProduct = localProduct else {
                        observer.on(.next(nil))
                        observer.on(.completed)
                        return Disposables.create()
                    }
                    
                    let product = localProduct.toProduct()
                    
                    observer.on(.next(product))
                    observer.on(.completed)
                } catch {
                    observer.on(.error(error))
                }
                
                return Disposables.create()
            }
        }
    }
    
    // MARK: - WriterDataSource methods
    
    public func replaceProducts(_ products: [Product]) -> Observable<[Product]> {
        return .deferred {
            .create { [weak self, products] observer in
                do {
                    let localProducts = products.map(LocalProduct.init(product:))
                    guard let data = try self?.jsonEncoder.encode(localProducts) else {
                        observer.on(.next([]))
                        observer.on(.completed)
                        return Disposables.create()
                    }
                    
                    self?.userDefaults.setValue(data, forKey: Constants.productsKey)
                    observer.on(.next(products))
                    observer.on(.completed)
                } catch {
                    observer.on(.error(error))
                }
                
                return Disposables.create()
            }
        }
    }
    
    public func selectProduct(_ product: Product?) -> Observable<Product?> {
        return .deferred {
            .create { [weak self, product] observer in
                do {
                    guard let product = product else {
                        self?.userDefaults.setValue(nil, forKey: Constants.selectedProductKey)
                        observer.on(.next(nil))
                        observer.on(.completed)
                        return Disposables.create()
                    }
                    
                    let localProduct = LocalProduct(product: product)
                    
                    guard let data = try self?.jsonEncoder.encode(localProduct) else {
                        self?.userDefaults.setValue(nil, forKey: Constants.selectedProductKey)
                        observer.on(.next(nil))
                        observer.on(.completed)
                        return Disposables.create()
                    }
                    
                    self?.userDefaults.setValue(data, forKey: Constants.selectedProductKey)
                    observer.on(.next(product))
                    observer.on(.completed)
                } catch {
                    observer.on(.error(error))
                }
                
                return Disposables.create()
            }
        }
    }
}
