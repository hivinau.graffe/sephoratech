// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Foundation
import RxSwift

public struct ProductsRemoteDataSourceAdapter: ReaderDataSource {
    
    // MARK: - Properties
    
    private let session: Session
    private let request: GetProductsRequest
    
    // MARK: - Init
    
    public init(session: Session,
                request: GetProductsRequest) {
        self.session = session
        self.request = request
    }
    
    // MARK: - ReaderDataSource methods
    
    public func fetchProducts() -> Observable<[Product]> {
        let remoteProducts: Observable<[RemoteProduct]> = session.sendRequest(request)
        return remoteProducts.map { $0.map { $0.toProduct() } }
    }
    
    public func fetchSelectedProduct() -> Observable<Product?> {
        return .empty()
    }
}
