// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Foundation
import RxSwift

public final class SessionAdapter: NSObject, Session {
    
    // MARK: - Private properties
    
    private let session: URLSessionWrapper
    private let jsonDecoder: JSONDecoderWrapper
    private var currentTask: URLSessionDataTaskWrapper? {
        didSet {
            currentTask?.resume()
        }
    }
    
    // MARK: - Init
    
    public init(session: URLSessionWrapper,
                jsonDecoder: JSONDecoderWrapper) {
        self.session = session
        self.jsonDecoder = jsonDecoder
        super.init()
    }
    
    // MARK: - Session methods
    
    public func sendRequest<T>(_ request: Request) -> Observable<T> where T : Decodable {
        return .create { [weak self] observer in
            self?.currentTask?.cancel()
            self?.currentTask = self?.session
                .dataTask(with: request) { [weak self, observer] data, response, error in
                    if let error = error {
                        observer.on(.error(error))
                        return
                    }
                    
                    let urlResponse = response as? HTTPURLResponse
                    let statusCode = urlResponse?.statusCode ?? SessionStatus.SERVER_ERROR.rawValue
                    guard statusCode == SessionStatus.OK.rawValue else {
                        observer.on(.error(SessionError.receiveHttpErrorCode(statusCode)))
                        return
                    }
                    
                    guard let data = data,
                          let self = self else {
                        observer.on(.error(SessionError.cannotDecodeObject))
                        return
                    }
                    
                    do {
                        let decoded = try self.jsonDecoder.decode(T.self, from: data)
                        observer.on(.next(decoded))
                        observer.on(.completed)
                    } catch let error {
                        observer.on(.error(error))
                    }
                }
            return Disposables.create { [weak self] in
                self?.currentTask?.cancel()
                self?.currentTask = nil
            }
        }
    }
}
