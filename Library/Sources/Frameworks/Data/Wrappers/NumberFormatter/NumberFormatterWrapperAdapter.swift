// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Foundation

public struct NumberFormatterWrapperAdapter: NumberFormatterWrapper {
    
    // MARK: - NumberFormatterWrapper properties
    
    public var usesGroupingSeparator: Bool {
        get { numberFormatter.usesGroupingSeparator }
        set {
            numberFormatter.usesGroupingSeparator = newValue
        }
    }
    
    public var numberStyle: NumberFormatter.Style {
        get { numberFormatter.numberStyle }
        set {
            numberFormatter.numberStyle = newValue
        }
    }
    
    public var locale: Locale {
        get { numberFormatter.locale }
        set {
            numberFormatter.locale = newValue
        }
    }
    
    // MARK: - Properties
    
    private let numberFormatter: NumberFormatter
    
    // MARK: - Init
    
    public init(numberFormatter: NumberFormatter) {
        self.numberFormatter = numberFormatter
    }
    
    // MARK: - NumberFormatterWrapper methods
    
    public func string(from value: Double) -> String? {
        return numberFormatter.string(from: value as NSNumber)
    }
}
