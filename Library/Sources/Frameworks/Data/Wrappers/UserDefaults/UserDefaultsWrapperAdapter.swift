// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Foundation

public struct UserDefaultsWrapperAdapter: UserDefaultsWrapper {
    
    // MARK: - Properties
    
    private let userDefaults: UserDefaults
    
    // MARK: - Init
    
    public init(userDefaults: UserDefaults) {
        self.userDefaults = userDefaults
    }
    
    // MARK: - UserDefaultsWrapper methods
    
    public func setValue(_ value: Any?, forKey key: String) {
        userDefaults.setValue(value, forKey: key)
    }
    
    public func value(forKey key: String) -> Any? {
        return userDefaults.value(forKey: key)
    }
}
