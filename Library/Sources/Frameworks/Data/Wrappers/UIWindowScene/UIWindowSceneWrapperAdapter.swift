// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import UIKit

public struct UIWindowSceneWrapperAdapter: UIWindowSceneWrapper {
    
    // MARK: - Properties
    
    private let windowScene: UIWindowScene
    
    // MARK: - Init
    
    public init(windowScene: UIWindowScene) {
        self.windowScene = windowScene
    }
    
    // MARK: - UIWindowSceneWrapper methods
    
    public func window() -> UIWindowWrapper {
        let window = UIWindow(windowScene: windowScene)
        if #available(iOS 15.0, *) {
            return UIWindowWrapperAdapter(window: windowScene.keyWindow ?? window)
        } else {
            return UIWindowWrapperAdapter(window: window)
        }
    }
}
