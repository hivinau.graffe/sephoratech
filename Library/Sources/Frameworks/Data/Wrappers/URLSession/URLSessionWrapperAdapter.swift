// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Foundation

public struct URLSessionWrapperAdapter: URLSessionWrapper {
    
    // MARK: - Properties
    
    private let session: URLSession
    private let sessionDataTaskWrapperFactory: URLSessionDataTaskWrapperFactory
    
    // MARK: - Init
    
    public init(session: URLSession,
                sessionDataTaskWrapperFactory: URLSessionDataTaskWrapperFactory) {
        self.session = session
        self.sessionDataTaskWrapperFactory = sessionDataTaskWrapperFactory
    }
    
    // MARK: - URLSessionWrapper methods
    
    public func dataTask(with request: Request,
                         completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTaskWrapper? {
        do {
            let rawRequest = try request.raw()
            let sessionDataTask = session.dataTask(with: rawRequest, completionHandler: completionHandler)
            return sessionDataTaskWrapperFactory.build(from: sessionDataTask)
        }
        catch {
            return nil
        }
    }
}
