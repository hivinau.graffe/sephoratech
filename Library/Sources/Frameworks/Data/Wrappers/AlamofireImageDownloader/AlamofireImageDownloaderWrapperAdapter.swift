// Copyright © Hivinau GRAFFE. All rights reserved.

import AlamofireImage
import Domain
import Foundation
import UIKit

public typealias AFImageDownloader = AlamofireImage.ImageDownloader

public final class AlamofireImageDownloaderWrapperAdapter: NSObject, AlamofireImageDownloaderWrapper {
    
    // MARK: - Private properties
    
    private let imageDownloader: AFImageDownloader
    
    // MARK: - Init
    
    public init(imageDownloader: AFImageDownloader) {
        self.imageDownloader = imageDownloader
    }
    
    // MARK: - AlamofireImageDownloaderWrapper methods
    
    public func downloadImage(_ urlRequest: URLRequest, completion: @escaping (UIImage?) -> Void) {
        imageDownloader.download(urlRequest,
                                 completion: { [completion] response in
            guard case .success(let image) = response.result else {
                completion(nil)
                return
            }
            
            completion(image)
        })
    }
}
