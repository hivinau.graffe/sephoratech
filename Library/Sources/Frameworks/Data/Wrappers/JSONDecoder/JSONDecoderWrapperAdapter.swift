// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Foundation

public struct JSONDecoderWrapperAdapter: JSONDecoderWrapper {
    
    // MARK: - Properties
    
    private let jsonDecoder: JSONDecoder
    
    // MARK: - Init
    
    public init(jsonDecoder: JSONDecoder) {
        self.jsonDecoder = jsonDecoder
    }
    
    // MARK: - JSONDecoderWrapper methods
    
    public func decode<T>(_ type: T.Type, from data: Data) throws -> T where T : Decodable {
        try jsonDecoder.decode(T.self, from: data)
    }
}
