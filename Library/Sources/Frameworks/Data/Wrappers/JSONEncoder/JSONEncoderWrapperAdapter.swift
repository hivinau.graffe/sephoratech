// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Foundation

public struct JSONEncoderWrapperAdapter: JSONEncoderWrapper {
    
    // MARK: - Properties
    
    private let jsonEncoder: JSONEncoder
    
    // MARK: - Init
    
    public init(jsonEncoder: JSONEncoder) {
        self.jsonEncoder = jsonEncoder
    }
    
    // MARK: - JSONEncoderWrapper methods
    
    public func encode<T>(_ encodable: T) throws -> Data where T : Encodable {
        try jsonEncoder.encode(encodable)
    }
}
