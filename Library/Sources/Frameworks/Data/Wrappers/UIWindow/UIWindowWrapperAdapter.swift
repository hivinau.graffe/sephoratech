// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import UIKit

public struct UIWindowWrapperAdapter: UIWindowWrapper {
    
    // MARK: - Properties
    
    private let window: UIWindow
    
    // MARK: - Init
    
    public init(window: UIWindow) {
        self.window = window
    }
    
    // MARK: - UIWindowWrapper properties
    
    public var rootViewController: UIViewController? {
        get {
            window.rootViewController
        }
        set {
            window.rootViewController = newValue
        }
    }
    
    // MARK: - UIWindowWrapper methods
    
    public func makeKeyAndVisible() {
        window.makeKeyAndVisible()
    }
    
    public func raw() -> UIWindow {
        return window
    }
}
