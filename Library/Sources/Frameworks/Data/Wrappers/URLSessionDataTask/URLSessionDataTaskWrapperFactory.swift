// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Foundation

public final class URLSessionDataTaskWrapperFactory: NSObject {
    
    public func build(from sessionDataTask: URLSessionDataTask) -> URLSessionDataTaskWrapper {
        return URLSessionDataTaskWrapperAdapter(sessionDataTask: sessionDataTask)
    }
}
