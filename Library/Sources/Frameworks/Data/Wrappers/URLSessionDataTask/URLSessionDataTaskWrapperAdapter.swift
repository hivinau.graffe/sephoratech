// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Foundation

public struct URLSessionDataTaskWrapperAdapter: URLSessionDataTaskWrapper {
    
    // MARK: - Properties
    
    private let sessionDataTask: URLSessionDataTask
    
    // MARK: - Init
    
    public init(sessionDataTask: URLSessionDataTask) {
        self.sessionDataTask = sessionDataTask
    }
    
    // MARK: - URLSessionDataTaskWrapper methods
    
    public func resume() {
        sessionDataTask.resume()
    }
    
    public func cancel() {
        sessionDataTask.cancel()
    }
    
    public func raw() -> URLSessionDataTask {
        return sessionDataTask
    }
}
