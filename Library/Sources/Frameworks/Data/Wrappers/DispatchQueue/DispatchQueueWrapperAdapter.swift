// Copyright © Hivinau GRAFFE. All rights reserved.

import Dispatch
import Domain

public struct DispatchQueueWrapperAdapter: DispatchQueueWrapper {
    
    // MARK: - Properties
    
    private let dispatcher: Dispatcher
    
    // MARK: - Init
    
    public init(dispatcher: Dispatcher) {
        self.dispatcher = dispatcher
    }
    
    // MARK: - DispatchQueueWrapper methods
    
    public func async(execute work: @escaping () -> Void) {
        dispatcher.async(
            group: nil,
            qos: .unspecified,
            flags: [],
            execute: work
        )
    }
    
    public func raw() -> DispatchQueue {
        dispatcher.asDispatchQueue
    }
}
