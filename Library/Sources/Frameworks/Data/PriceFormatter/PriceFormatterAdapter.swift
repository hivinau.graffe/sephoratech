// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Foundation

public struct PriceFormatterAdapter: PriceFormatter {
    
    // MARK: - Properties
    
    private let numberFormatter: NumberFormatterWrapper
    
    // MARK: - Init
    
    public init(numberFormatter: NumberFormatterWrapper) {
        self.numberFormatter = numberFormatter
    }
    
    // MARK: - PriceFormatter methods
    
    public func format(_ value: Double) -> String? {
        return numberFormatter.string(from: value)
    }
}
