// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Foundation

public class BaseRequest: NSObject, Request {
    
    // MARK: - Private properties
    
    private let route: Route
    
    // MARK: - Init
    
    public init(route: Route) {
        self.route = route
        super.init()
    }
    
    // MARK: - SessionRequest methods
    
    public func raw() throws -> URLRequest {
        guard let url = route.url() else { throw SessionError.cannotCreateUrl }
        
        var request = URLRequest(url: url)
        request.httpMethod = route.method.rawValue
        return request
    }
}
