// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Foundation

public final class GetProductsRequest: BaseRequest {
    
    // MARK: - Init
    
    public init(route: GetProductsRoute) {
        super.init(route: route)
    }
}
