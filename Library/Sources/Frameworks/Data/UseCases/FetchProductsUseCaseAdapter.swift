import Domain
import RxSwift

public struct FetchProductsUseCaseAdapter: FetchProductsUseCase {
    
    // MARK: - Private properties
    
    private let productsRepository: ProductsRepository
    
    // MARK: - Init
    
    public init(productsRepository: ProductsRepository) {
        self.productsRepository = productsRepository
    }
    
    // MARK: - FetchProductsUseCase methods
    
    public func start() -> Observable<[Product]> {
        
        let productsObservable = productsRepository
            .fetchProducts()
            .share(replay: 1)
        
        let specialProductsObservable = productsObservable
            .map { products in
                products.filter(\.isSpecialBrand)
            }
        
        let standardProductsObservable = productsObservable
            .map { products in
                products.filter { !$0.isSpecialBrand }
            }
        
        return Observable.combineLatest(specialProductsObservable, standardProductsObservable)
            .map(+)
    }
}
