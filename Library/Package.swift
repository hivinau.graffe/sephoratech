// swift-tools-version: 5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Library",
    defaultLocalization: "fr",
    platforms: [
        .iOS(.v13)
    ],
    products: CleanArchiFramework.allCases.map(\.product),
    dependencies: [
        .package(url: "https://github.com/ReactiveX/RxSwift.git", exact: "6.5.0"),
        .package(url: "https://github.com/Alamofire/AlamofireImage.git", exact: "4.2.0"),
        .package(url: "https://github.com/Quick/Nimble", exact: "12.0.0"),
        .package(url: "https://github.com/MakeAWishFoundation/SwiftyMocky", exact: "4.2.0"),
        .package(url: "https://github.com/RxSwiftCommunity/Action", exact: "5.0.0")
    ],
    targets: CleanArchiFramework.allCases.map(\.target) + CleanArchiFramework.allCases.flatMap(\.testsTargets)
)

enum CleanArchiFramework: String, CaseIterable {
    case Data
    case Domain
    
    var path: String { "Sources/Frameworks/\(rawValue)" }
    var testsPath: String { "Tests/Frameworks/\(rawValue)Tests" }
    var testsName: String { "\(rawValue)Tests" }
    var product: Product { Product.Library.library(framework: self) }
}

extension CleanArchiFramework {
    var target: Target {
        .target(framework: self, dependencies: dependencies, resources: resources)
    }
    
    var testsTargets: [Target] {
        switch self {
        case .Data, .Domain:
            return [.testTarget(framework: self, dependencies: testsDependencies, resources: testsResources)]
        }
    }
    
    var dependencies: [Target.Dependency] {
        switch self {
        case .Data:
            return [
                .framework(.Domain),
                .external(.Action),
                .external(.RxCocoa),
                .external(.RxSwift)
            ]
        case .Domain:
            return [
                .external(.Action),
                .external(.AlamofireImage),
                .external(.RxCocoa),
                .external(.RxSwift)
            ]
        }
    }
    
    var testsDependencies: [Target.Dependency] {
        [
            .framework(self),
            .external(.AlamofireImage),
            .external(.Nimble),
            .external(.SwiftyMocky),
            .external(.RxCocoa),
            .external(.RxSwift)
        ]
    }
    
    var resources: [Resource]? {
        switch self {
        case .Data:
            return [.process("Resources/")]
        default: return nil
        }
    }
    
    var testsResources: [Resource]? {
        switch self {
        case .Data:
            return [.process("Resources/")]
        default: return nil
        }
    }
    
    var libraryType: Product.Library.LibraryType? {
        nil
    }
}

enum ExternalLib: String {
    case Action
    case AlamofireImage
    case Nimble
    case SwiftyMocky
    case RxCocoa
    case RxSwift
    
    var dependency: Target.Dependency {
        switch self {
        case .RxSwift, .AlamofireImage, .Nimble, .Action:
            return .byName(name: rawValue)
        case .SwiftyMocky:
            return .product(name: "SwiftyMocky", package: "swiftymocky")
        case .RxCocoa:
            return .product(name: "RxCocoa", package: "RxSwift")
        }
    }
}

extension Target.Dependency {
    
    static func framework(_ framework: CleanArchiFramework) -> Target.Dependency {
        Target.Dependency(stringLiteral: framework.rawValue)
    }
    
    static func external(_ externalLib: ExternalLib) -> Target.Dependency {
        externalLib.dependency
    }
}

extension Product.Library {
    
    static func library(framework: CleanArchiFramework) -> Product {
        .library(name: framework.rawValue, type: framework.libraryType, targets: [framework.rawValue])
    }
}

extension Target {
    
    static func target(
        framework: CleanArchiFramework,
        dependencies: [Target.Dependency] = [],
        exclude: [String] = [],
        sources: [String]? = nil,
        resources: [Resource]? = nil,
        publicHeadersPath: String? = nil,
        cSettings: [CSetting]? = nil,
        cxxSettings: [CXXSetting]? = nil,
        swiftSettings: [SwiftSetting]? = nil,
        linkerSettings: [LinkerSetting]? = nil,
        plugins: [PluginUsage]? = nil
    ) -> Target {
        .target(
            name: framework.rawValue,
            dependencies: dependencies,
            path: framework.path,
            exclude: exclude,
            sources: sources,
            resources: resources,
            publicHeadersPath: publicHeadersPath,
            cSettings: cSettings,
            cxxSettings: cxxSettings,
            swiftSettings: swiftSettings,
            linkerSettings: linkerSettings,
            plugins: plugins
        )
    }
    
    static func testTarget(
        framework: CleanArchiFramework,
        dependencies: [Target.Dependency] = [],
        exclude: [String] = [],
        sources: [String]? = nil,
        resources: [Resource]? = nil,
        cSettings: [CSetting]? = nil,
        cxxSettings: [CXXSetting]? = nil,
        swiftSettings: [SwiftSetting]? = nil,
        linkerSettings: [LinkerSetting]? = nil,
        plugins: [Target.PluginUsage]? = nil
    ) -> Target {
        .testTarget(
            name: framework.testsName,
            dependencies: dependencies,
            path: framework.testsPath,
            exclude: exclude,
            sources: sources,
            resources: resources,
            cSettings: cSettings,
            cxxSettings: cxxSettings,
            swiftSettings: swiftSettings,
            linkerSettings: linkerSettings,
            plugins: plugins
        )
    }
}

