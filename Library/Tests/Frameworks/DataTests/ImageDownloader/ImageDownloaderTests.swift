// Copyright © Hivinau GRAFFE. All rights reserved.

import Nimble
import UIKit
import XCTest
@testable import Data

final class ImageDownloaderTests: XCTestCase {
    
    // MARK: - Private properties
    
    private var tested: ImagesDownloaderAdapter!
    private let imageDownloader = AlamofireImageDownloaderWrapperMock()
    
    // MARK: - Lifecycle
    
    override func setUp() {
        tested = ImagesDownloaderAdapter(imageDownloader: imageDownloader)
        super.setUp()
    }
    
    // MARK: - Tests
    
    func testImagesDownloaderDownloadImageRequest_shouldBeCalled_whenSubscribeImageForValidUrlString() {
        _ = tested.image(for: "www.google.com")
            .subscribe(onNext: { _ in })
        
        imageDownloader.verifyEventually(test: self, .downloadImage(.any, completion: .any))
    }
    
    func testImagesDownloaderDownloadImageRequest_shouldNeverBeCalled_whenSubscribeImageForInvalidUrlString() {
        _ = tested.image(for: "")
            .subscribe(onNext: { _ in })
        
        imageDownloader.verifyEventually(test: self, .downloadImage(.any, completion: .any), count: .never)
    }
}
