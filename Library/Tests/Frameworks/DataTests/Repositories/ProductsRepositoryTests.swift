// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Nimble
import RxSwift
import XCTest
@testable import Data

final class ProductsRepositoryTests: XCTestCase {
    
    // MARK: - Private properties
    
    private var tested: ProductsRepositoryAdapter!
    private let localDataSource: WriterReaderDataSourceMock = {
        let localDataSource = WriterReaderDataSourceMock()
        localDataSource.given(.fetchProducts(willReturn: Observable<[Product]>.mockInject()))
        return localDataSource
    }()
    private lazy var remoteDataSource: ReaderDataSourceMock = {
        let remoteDataSource = ReaderDataSourceMock()
        remoteDataSource.given(.fetchProducts(willReturn: Observable<[Product]>.mockInject()))
        return remoteDataSource
    }()
    
    // MARK: - Lifecycle
    
    override func setUp() {
        tested = ProductsRepositoryAdapter(remoteDataSource: remoteDataSource,
                                           localDataSource: localDataSource)
        super.setUp()
    }
    
    // MARK: - Tests
    
    func testLocalDataSourceFetchProducts_shouldBeCalled_whenFetchProductsCalled() {
        _ = tested.fetchProducts()
        
        localDataSource.verifyEventually(test: self, .fetchProducts())
    }
    
    func testRemoteDataSourceFetchProducts_shouldBeCalled_whenFetchProductsCalled() {
        _ = tested.fetchProducts()
        
        remoteDataSource.verifyEventually(test: self, .fetchProducts())
    }
    
//    func testLocalDataSourceReplaceProducts_shouldBeCalled_whenFetchProductsCalled() {
//        localDataSource.fetchProductsResultSubject?.on(.next([]))
//        remoteDataSource.fetchProducts().underlyingObserver.on(.next([]))
//
//        _ = tested.fetchProducts()
//            .subscribe(onNext: { _ in })
//
//        expect(self.localDataSource.replaceProductsCalled).toEventually(beTrue())
//    }
    
    func testFetchProductsSubscribe_shouldNeverOnNextProducts_whenLocalDataSourceFetchProductsNeverOnNextProducts() {
        var _products: [Product]?
        
        _ = tested.fetchProducts()
            .subscribe(onNext: {
                _products = $0
            })
        
        expect(_products).toEventually(beNil())
    }
    
//    func testFetchProductsSubscribe_shouldOnNextProducts_whenLocalDataSourceFetchProductsOnNextProducts() {
//        let products = [Product(id: .zero,
//                                name: nil,
//                                descriptionFr: nil,
//                                price: .zero,
//                                isProductSet: false,
//                                isSpecialBrand: false,
//                                imagesUrl: nil)]
//        localDataSource.fetchProductsResultSubject = PublishSubject()
//        localDataSource.fetchProductsResultSubject?.on(.next(products))
//        var _products: [Product]?
//
//        _ = tested.fetchProducts()
//            .subscribe(onNext: {
//                _products = $0
//            })
//
//        expect(_products).toNotEventually(beNil())
//    }
    
    func testFetchProductsSubscribe_shouldNeverOnNextProducts_whenLocalDataSourceReplaceProductsNeverOnNextProducts() {
        var _products: [Product]?
        
        _ = tested.fetchProducts()
            .subscribe(onNext: {
                _products = $0
            })
        
        expect(_products).toEventually(beNil())
    }
    
    func testFetchProductsSubscribe_shouldNeverOnNextProducts_whenLocalDataSourceReplaceProductsNeverOnNextProductsAndRemoteDataSourceFetchProductsNeverCalled() {
        let products = [Product(id: .zero,
                                name: nil,
                                descriptionFr: nil,
                                price: .zero,
                                isProductSet: false,
                                isSpecialBrand: false,
                                imagesUrl: nil)]
        localDataSource.given(.replaceProducts(.any, willReturn: .of(products)))
        var _products: [Product]?
        
        _ = tested.fetchProducts()
            .subscribe(onNext: {
                _products = $0
            })
        
        expect(_products).toEventually(beNil())
    }
    
//    func testFetchProductsSubscribe_shouldOnNextProducts_whenLocalDataSourceReplaceProductsOnNextProductsAndRemoteDataSourceFetchProductsNeverCalled() {
//        let products = [Product(id: .zero,
//                                name: nil,
//                                descriptionFr: nil,
//                                price: .zero,
//                                isProductSet: false,
//                                isSpecialBrand: false,
//                                imagesUrl: nil)]
//        localDataSource.replaceProductsResultSubject = PublishSubject()
//        localDataSource.replaceProductsResultSubject?.on(.next(products))
//        remoteDataSource.fetchProducts().underlyingObserver.on(.next([]))
//        var _products: [Product]?
//        
//        _ = tested.fetchProducts()
//            .subscribe(onNext: {
//                _products = $0
//            })
//        
//        expect(_products).toNotEventually(beNil())
//    }
}
