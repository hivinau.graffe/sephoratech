// Copyright © Hivinau GRAFFE. All rights reserved.

import Nimble
import XCTest
@testable import Data

final class GetProductsRouteTests: XCTestCase {
    
    // MARK: - Constants
    
    private enum Constants {
        static let endpoint = "endPoint"
        static let routeTemplate = "%@/items.json"
    }
    
    // MARK: - Private properties
    
    private var tested: GetProductsRoute!
    
    // MARK: - Lifecycle
    
    override func setUp() {
        tested = GetProductsRoute(endpoint: Constants.endpoint)
        super.setUp()
    }
    
    // MARK: - Tests
    
    func testUrl_shouldReturnExpectedUrlString() {
        let urlString = String(format: Constants.routeTemplate, Constants.endpoint)
        let url = tested.url()
        
        expect(url?.absoluteString).to(equal(urlString))
    }
}
