// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Nimble
import XCTest
@testable import Data

final class BaseRequestTests: XCTestCase {
    
    // MARK: - Private properties
    
    private var tested: BaseRequest!
    private let route = RouteMock()
    
    // MARK: - Lifecycle
    
    override func setUp() {
        tested = BaseRequest(route: route)
        super.setUp()
    }
    
    // MARK: - Tests
    
    func testRouteUrl_shouldBeCalled_whenRawCalled() throws {
        route.given(.url(willReturn: URL(string: "www.google.com")!))
        route.given(.method(getter: .get))
        
        _ = try tested.raw()
        
        route.verify(.url())
    }
    
    func testRaw_shouldThrowASessionError_whenRouteUrlReturnsNil () {
        route.given(.url(willReturn: nil))
        var error: Error?
        
        do {
            _ = try tested.raw()
        } catch let _error {
            error = _error
        }
        
        expect(error).toNot(beNil())
        expect(error).to(beAnInstanceOf(SessionError.self))
    }
}
