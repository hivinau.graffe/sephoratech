// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Nimble
import XCTest
@testable import Data

final class GetProductsRequestTests: XCTestCase {
    
    // MARK: - Constants
    
    private enum Constants {
        static let endpoint = "endPoint"
        static let routeTemplate = "%@/items.json"
    }
    
    // MARK: - Private properties
    
    private var tested: GetProductsRequest!
    private let route = GetProductsRoute(endpoint: Constants.endpoint)
    
    // MARK: - Lifecycle
    
    override func setUp() {
        tested = GetProductsRequest(route: route)
        super.setUp()
    }
    
    // MARK: - Tests
    
    func testRaw_shouldReturnExpectedRequest_whenRouteUrlReturnsValidUrlAndGetMethod() throws {
        let urlString = String(format: Constants.routeTemplate, Constants.endpoint)
        
        let request = try tested.raw()
        
        expect(request).toNot(beNil())
        expect(request.httpMethod).to(equal(HttpMethod.get.rawValue))
        expect(request.url?.absoluteString).to(equal(urlString))
    }
}
