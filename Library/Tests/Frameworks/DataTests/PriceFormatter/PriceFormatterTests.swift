// Copyright © Hivinau GRAFFE. All rights reserved.

import Nimble
import XCTest
@testable import Data

final class PriceFormatterTests: XCTestCase {
    
    // MARK: - Private properties
    
    private var tested: PriceFormatterAdapter!
    private let numberFormatter = NumberFormatterWrapperMock()
    
    // MARK: - Lifecycle
    
    override func setUp() {
        tested = PriceFormatterAdapter(numberFormatter: numberFormatter)
        super.setUp()
    }
    
    // MARK: - Tests
    
    func testNumberFormatterStringFromNumber_shouldBeCalled_whenFormatValueCalled() {
        _ = tested.format(.zero)
        
        numberFormatter.verify(.string(from: .any))
    }
}
