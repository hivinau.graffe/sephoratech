// Copyright © Hivinau GRAFFE. All rights reserved.

import Nimble
import XCTest
@testable import Data

final class FetchProductsUseCaseTests: XCTestCase {
    
    // MARK: - Private properties
    
    private var tested: FetchProductsUseCaseAdapter!
    private let productsRepository = ProductsRepositoryMock()
    
    // MARK: - Lifecycle
    
    override func setUp() {
        tested = FetchProductsUseCaseAdapter(productsRepository: productsRepository)
        super.setUp()
    }
    
    // MARK: - Tests
    
    func testProductsRepositoryFetchProducts_shouldBeCalled_whenStart() {
        productsRepository.given(.fetchProducts(willReturn: .of([])))
        
        _ = tested.start()
        
        productsRepository.verify(.fetchProducts())
    }
}
