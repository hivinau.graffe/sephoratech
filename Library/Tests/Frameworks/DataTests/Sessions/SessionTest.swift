// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Nimble
import RxSwift
import XCTest
@testable import Data

final class SessionTest: XCTestCase {
    
    // MARK: - Private properties
    
    private var tested: SessionAdapter!
    private let jsonDecoder = JSONDecoderWrapperMock()
    private let sessionDataTask = URLSessionDataTaskWrapperMock()
    private let request = RequestMock()
    private lazy var session: URLSessionWrapperMock = {
        let session = URLSessionWrapperMock()
        session.given(.dataTask(with: .any, completionHandler: .any, willReturn: sessionDataTask))
        return session
    }()
    
    // MARK: - Lifecycle
    
    override func setUp() {
        tested = SessionAdapter(session: session, jsonDecoder: jsonDecoder)
        super.setUp()
    }
    
    // MARK: - Tests
    
    func testSessionDataTaskCancel_shouldBeCalled_whenSendRequestCalledTwiceAtLeast() {
        let observable1 = tested.sendRequest(request) as! Observable<DecodableMock>
        _ = observable1.subscribe(onNext: { _ in })
        let observable2 = tested.sendRequest(request) as! Observable<DecodableMock>
        _ = observable2.subscribe(onNext: { _ in })
        
        sessionDataTask.verifyEventually(test: self, .cancel())
    }
    
    func testSessionDataTaskCancel_shouldBeCalled_whenDisposeSendRequestResultObservable() {
        let disposable = CompositeDisposable()
        let observable = tested.sendRequest(request) as! Observable<DecodableMock>
        let key = disposable.insert(observable.subscribe(onNext: { _ in }))
        
        disposable.remove(for: key!)
        
        sessionDataTask.verifyEventually(test: self, .cancel())
    }
    
    func testSendRequest_shouldReturnAnError_whenSessionPerformsCompletionWithError() {
        var error: Error?
        session.perform(.dataTask(with: .any,
                                  completionHandler: .any,
                                  perform: { request, completion in
            completion(nil, nil, ErrorMock.any)
        }))
        
        let observable = tested.sendRequest(request) as! Observable<DecodableMock>
        _ = observable.subscribe(onError: { error = $0 })
        
        expect(error).toEventually(beAnInstanceOf(ErrorMock.self))
    }

    func testSendRequest_shouldReturnASessionError_whenSessionPerformsCompletionWithUrlResponseStatusCodeEquals500() {
        var error: Error?
        session.perform(.dataTask(with: .any, completionHandler: .any, perform: { request, completion in
            let response = HTTPURLResponse(url: URL(string: "www.google.com")!,
                                           statusCode: SessionStatus.SERVER_ERROR.rawValue,
                                           httpVersion: nil,
                                           headerFields: nil)
            completion(nil, response, nil)
        }))
        
        let observable = tested.sendRequest(request) as! Observable<DecodableMock>
        _ = observable.subscribe(onError: { error = $0 })
        
        expect(error).toEventually(beAnInstanceOf(SessionError.self))
    }
    
    func testSendRequest_shouldReturnASessionError_whenSessionPerformsCompletionWithUrlResponseStatusCodeEquals200AndDataEqualsNil() {
        var error: Error?
        session.perform(.dataTask(with: .any, completionHandler: .any, perform: { request, completion in
            let response = HTTPURLResponse(url: URL(string: "www.google.com")!,
                                           statusCode: SessionStatus.OK.rawValue,
                                           httpVersion: nil,
                                           headerFields: nil)
            completion(nil, response, nil)
        }))
        
        let observable = tested.sendRequest(request) as! Observable<DecodableMock>
        _ = observable.subscribe(onError: { error = $0 })
        
        expect(error).toEventually(beAnInstanceOf(SessionError.self))
    }
    
    func testSendRequest_shouldReturnADecodable_whenSessionPerformsCompletionWithUrlResponseStatusCodeEquals200AndValidData() {
        var _decodable: DecodableMock?
        session.perform(.dataTask(with: .any, completionHandler: .any, perform: { request, completion in
            let data = DataMocker().data(from: "test")
            let response = HTTPURLResponse(url: URL(string: "www.google.com")!,
                                           statusCode: SessionStatus.OK.rawValue,
                                           httpVersion: nil,
                                           headerFields: nil)
            completion(data, response, nil)
        }))
        let decodable = DecodableMock()
        jsonDecoder.given(.decode(.any, from: .any, willReturn: decodable))
        
        let observable = tested.sendRequest(request) as! Observable<DecodableMock>
        _ = observable.subscribe(onNext: { _decodable = $0 })
        
        expect(_decodable).toEventually(equal(decodable))
    }
        
    func testSessionDataTaskResume_shouldBeCalled_whenSendRequestCalled() {
        session.perform(.dataTask(with: .any, completionHandler: .any, perform: { request, completion in
            let data = DataMocker().data(from: "test")
            let response = HTTPURLResponse(url: URL(string: "www.google.com")!,
                                           statusCode: SessionStatus.OK.rawValue,
                                           httpVersion: nil,
                                           headerFields: nil)
            completion(data, response, nil)
        }))
        let decodable = DecodableMock()
        jsonDecoder.given(.decode(.any, from: .any, willReturn: decodable))
        
        let observable = tested.sendRequest(request) as! Observable<DecodableMock>
        _ = observable.subscribe(onNext: { _ in })
        
        sessionDataTask.verifyEventually(test: self, .resume())
    }
}
