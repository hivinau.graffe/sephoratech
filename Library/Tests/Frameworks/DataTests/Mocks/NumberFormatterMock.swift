// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

final class NumberFormatterMock: NumberFormatter {
    
    var stringMock: String?
    var stringFromNumberCallsCount = 0
    var stringFromNumberCalled: Bool {
        stringFromNumberCallsCount > 0
    }
    
    var usesGroupingSeparatorDidSetCallsCount = 0
    var usesGroupingSeparatorDidSetCalled: Bool {
        usesGroupingSeparatorDidSetCallsCount > 0
    }
    
    var numberStyleDidSetCallsCount = 0
    var numberStyleDidSetCalled: Bool {
        numberStyleDidSetCallsCount > 0
    }
    
    var localeDidSetCallsCount = 0
    var localeDidSetCalled: Bool {
        localeDidSetCallsCount > 0
    }
    
    override var usesGroupingSeparator: Bool {
        didSet { usesGroupingSeparatorDidSetCallsCount += 1 }
    }
    
    override var numberStyle: NumberFormatter.Style {
        didSet { numberStyleDidSetCallsCount += 1 }
    }
    
    override var locale: Locale? {
        didSet { localeDidSetCallsCount += 1 }
    }

    override func string(from number: NSNumber) -> String? {
        stringFromNumberCallsCount += 1
        return stringMock
    }
}
