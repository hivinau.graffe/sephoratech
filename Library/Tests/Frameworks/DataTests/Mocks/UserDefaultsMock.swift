// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

final class UserDefaultsMock: UserDefaults {
    
    var setValueCallsCount = 0
    var setValueCalled: Bool {
        setValueCallsCount > 0
    }
    
    var valueMock: Any?
    var valueForKeyCallsCount = 0
    var valueForKeyCalled: Bool {
        valueForKeyCallsCount > 0
    }

    override func setValue(_ value: Any?, forKey key: String) {
        setValueCallsCount += 1
    }
    
    override func value(forKey key: String) -> Any? {
        valueForKeyCallsCount += 1
        return valueMock
    }
}
