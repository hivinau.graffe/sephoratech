// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

final class JSONEncoderMock: JSONEncoder {
    
    var dataMock: Data!
    var encodeEncodableFromDataCallsCount = 0
    var encodeEncodableFromDataCalled: Bool {
        encodeEncodableFromDataCallsCount > 0
    }
    
    override func encode<T>(_ value: T) throws -> Data where T : Encodable {
        encodeEncodableFromDataCallsCount += 1
        return dataMock
    }
}
