// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

final class JSONDecoderMock: JSONDecoder {
    
    var decodableMock: Decodable!
    var decodeDecodableFromDataCallsCount = 0
    var decodeDecodableFromDataCalled: Bool {
        decodeDecodableFromDataCallsCount > 0
    }
    
    override func decode<T>(_ type: T.Type, from data: Data) throws -> T where T : Decodable {
        decodeDecodableFromDataCallsCount += 1
        return decodableMock as! T
    }
}
