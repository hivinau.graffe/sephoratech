// Copyright © Hivinau GRAFFE. All rights reserved.

import Alamofire
import AlamofireImage
import Foundation

final class AFImageDownloaderMock: AlamofireImage.ImageDownloader {
    
    var imageResponse: AFIDataResponse<Image>?
    var downloadRequestCallsCount = 0
    var downloadRequestCalled: Bool {
        downloadRequestCallsCount > 0
    }
    
    override func download(_ urlRequest: URLRequestConvertible,
                           cacheKey: String? = nil,
                           receiptID: String = UUID().uuidString,
                           serializer: ImageResponseSerializer? = nil,
                           filter: ImageFilter? = nil,
                           progress: ImageDownloader.ProgressHandler? = nil,
                           progressQueue: DispatchQueue = DispatchQueue.main,
                           completion: ImageDownloader.CompletionHandler? = nil) -> RequestReceipt? {
        guard let imageResponse = imageResponse else {
            return nil
        }
        
        completion?(imageResponse)
        downloadRequestCallsCount += 1
        return nil
    }
}
