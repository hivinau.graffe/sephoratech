// Copyright © Hivinau GRAFFE. All rights reserved.

import UIKit

final class UIWindowMock: UIWindow {
    
    var makeKeyAndVisibleCallsCount = 0
    var makeKeyAndVisibleCalled: Bool {
        makeKeyAndVisibleCallsCount > 0
    }
    
    var rootViewControllerMock: UIViewController?
    override var rootViewController: UIViewController? {
        get {
            rootViewControllerMock
        }
        set {
            rootViewControllerMock = newValue
        }
    }
    
    override func makeKeyAndVisible() {
        makeKeyAndVisibleCallsCount += 1
    }
}
