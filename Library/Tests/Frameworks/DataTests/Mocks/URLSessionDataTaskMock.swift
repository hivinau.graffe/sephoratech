// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

@available(iOS, introduced: 13.0, deprecated)
final class URLSessionDataTaskMock: URLSessionDataTask {
    
    var resumeCallsCount = 0
    var resumeCalled: Bool {
        resumeCallsCount > 0
    }
    
    var cancelCallsCount = 0
    var cancelCalled: Bool {
        cancelCallsCount > 0
    }
    
    override func resume() {
        resumeCallsCount += 1
    }
    
    override func cancel() {
        cancelCallsCount += 1
    }
}
