// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

struct DataMocker {
    
    func data(from string: String) -> Data? {
        return string.data(using: .utf8)
    }
}
