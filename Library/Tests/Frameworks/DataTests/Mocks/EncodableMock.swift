// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

final class EncodableMock: Encodable, Equatable {

    func encode(to encoder: Encoder) throws {
    }
    
    static func == (lhs: EncodableMock, rhs: EncodableMock) -> Bool {
        true
    }
}
