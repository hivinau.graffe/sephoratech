// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

struct URLRequestMocker {
    
    func request() -> URLRequest? {
        return .init(url: URL(string: "www.google.com")!)
    }
}
