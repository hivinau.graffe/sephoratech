// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

@available(iOS, introduced: 13.0, deprecated)
final class URLSessionMock: URLSession {
    
    private let useNestedSession: Bool
    private let nestedSession = URLSession(configuration: .default)
    
    var sessionDataTaskMock: URLSessionDataTask!
    var dataTaskWithRequestCallsCount = 0
    var dataTaskWithRequestCalled: Bool {
        dataTaskWithRequestCallsCount > 0
    }
    
    init(useNestedSession: Bool) {
        self.useNestedSession = useNestedSession
    }
    
    override func dataTask(with request: URLRequest,
                           completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        dataTaskWithRequestCallsCount += 1
        if useNestedSession {
            return nestedSession.dataTask(with: request, completionHandler: completionHandler)
        }
        
        return sessionDataTaskMock
    }
}
