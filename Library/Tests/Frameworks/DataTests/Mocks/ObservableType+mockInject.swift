// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation
import RxCocoa
import RxSwift

private var xoSubjectAssociationKey: UInt8 = 0
private var xoSpyAssociationKey: UInt8 = 1

extension ObservableType {
    
    static func mockInject() -> Observable<Element> {
        let subject = PublishSubject<Element>()
        let observable = subject.asObservable()
        observable.storeSubject(newValue: subject)
        return observable
    }
    
    func storeSubject(newValue: PublishSubject<Element>) {
        objc_setAssociatedObject(self, &xoSubjectAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
    }
    
    var underlyingObserver: PublishSubject<Element> {
        get {
            guard let result = objc_getAssociatedObject(self, &xoSubjectAssociationKey) as? PublishSubject<Element> else {
                assertionFailure("Observable must be created with Observable<T>.mockInject() to make magic happen")
                return PublishSubject<Element>()
            }
            return result
        }
    }
}
