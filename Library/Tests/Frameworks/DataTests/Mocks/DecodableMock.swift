// Copyright © Hivinau GRAFFE. All rights reserved.

import Foundation

final class DecodableMock: Decodable, Equatable {

    convenience init(from decoder: Decoder) throws {
        self.init()
    }
    
    static func == (lhs: DecodableMock, rhs: DecodableMock) -> Bool {
        true
    }
}
