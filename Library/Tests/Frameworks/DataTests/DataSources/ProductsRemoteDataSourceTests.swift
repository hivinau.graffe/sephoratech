// Copyright © Hivinau GRAFFE. All rights reserved.

import Domain
import Nimble
import RxSwift
import XCTest
@testable import Data

final class ProductsRemoteDataSourceTests: XCTestCase {
    
    // MARK: - Private properties
    
    private var tested: ProductsRemoteDataSourceAdapter!
    private let route = GetProductsRoute(endpoint: "endpoint")
    private lazy var request = GetProductsRequest(route: route)
    private lazy var session: SessionMock = {
        let session = SessionMock()
        session.given(.sendRequest(.any, willReturn: Observable<[RemoteProduct]>.mockInject()))
        return session
    }()
    
    // MARK: - Lifecycle
    
    override func setUp() {
        tested = ProductsRemoteDataSourceAdapter(session: session,
                                                 request: request)
        super.setUp()
    }
    
    // MARK: - Tests
    
    func testSessionSendRequest_shouldBeCalled_whenFetchProductsCalled() {
        _ = tested.fetchProducts()
        
        session.verify(.sendRequest(.any))
    }
}
