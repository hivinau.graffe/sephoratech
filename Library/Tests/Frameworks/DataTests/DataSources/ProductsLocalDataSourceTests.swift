// Copyright © Hivinau GRAFFE. All rights reserved.

import Nimble
import XCTest
@testable import Data

final class ProductsLocalDataSourceTests: XCTestCase {
    
    // MARK: - Private properties
    
    private var tested: ProductsLocalDataSourceAdapter!
    private let jsonDecoder = JSONDecoderWrapperMock()
    private let jsonEncoder = JSONEncoderWrapperMock()
    private let userDefaults = UserDefaultsWrapperMock()
    
    // MARK: - Lifecycle
    
    override func setUp() {
        tested = ProductsLocalDataSourceAdapter(jsonDecoder: jsonDecoder,
                                                jsonEncoder: jsonEncoder,
                                                userDefaults: userDefaults)
        super.setUp()
    }
    
    // MARK: - Tests
    
    func testSessionSendRequest_shouldBeCalled_whenFetchProductsCalled() {
       _ = tested.fetchProducts()
            .subscribe()
        
        userDefaults.verifyEventually(test: self, .value(forKey: .any))
    }
}
