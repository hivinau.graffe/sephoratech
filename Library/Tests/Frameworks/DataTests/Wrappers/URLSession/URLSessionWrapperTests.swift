// Copyright © Hivinau GRAFFE. All rights reserved.

import Nimble
import XCTest
@testable import Data

@available(iOS, introduced: 13.0, deprecated)
final class URLSessionWrapperTests: XCTestCase {

    // MARK: - Private properties
    
    private var tested: URLSessionWrapperAdapter!
    private var session = URLSessionMock(useNestedSession: true)
    private let sessionDataTaskWrapperFactory = URLSessionDataTaskWrapperFactory()
    private let requestMocker = URLRequestMocker()
    private let request = RequestMock()
    
    // MARK: - Lifecycle
    
    override func setUp() {
        tested = URLSessionWrapperAdapter(session: session,
                                          sessionDataTaskWrapperFactory: sessionDataTaskWrapperFactory)
        super.setUp()
    }
    
    // MARK: - Tests
    
    func testSessionDataTaskWithRequest_shouldBeCalled_whenDataTaskWithRequestCalledAndSessionMockDoesNotUseNestedSession() {
        session = URLSessionMock(useNestedSession: false)
        session.sessionDataTaskMock = URLSessionDataTaskMock()
        tested = URLSessionWrapperAdapter(session: session,
                                          sessionDataTaskWrapperFactory: sessionDataTaskWrapperFactory)
        request.given(.raw(willReturn: requestMocker.request()!))
        
        _ = tested.dataTask(with: request) { _,_,_ in }
        
        expect(self.session.dataTaskWithRequestCalled).to(beTrue())
    }
    
    func testSessionDataTaskWithRequest_shouldBeCalled_whenDataTaskWithRequestCalledAndSessionMockUsesNestedSessionCalled() {
        request.given(.raw(willReturn: requestMocker.request()!))
        
        _ = tested.dataTask(with: request) { _,_,_ in }
        
        expect(self.session.dataTaskWithRequestCalled).to(beTrue())
    }
    
    func testDataTask_shouldReturnNil_whenRequestRawCalledAndThowsError() {
        request.given(.raw(willThrow: ErrorMock.any))
        
        let task = tested.dataTask(with: request) { _,_,_ in }
        
        expect(task).to(beNil())
    }
}
