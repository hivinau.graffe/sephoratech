// Copyright © Hivinau GRAFFE. All rights reserved.

import Nimble
import XCTest
@testable import Data

final class UserDefaultsWrapperTests: XCTestCase {

    // MARK: - Private properties
    
    private var tested: UserDefaultsWrapperAdapter!
    private let userDefaults = UserDefaultsMock()

    // MARK: - Lifecycle

    override func setUp() {
        tested = UserDefaultsWrapperAdapter(userDefaults: userDefaults)
        super.setUp()
    }
    
    // MARK: - Tests
    
    func testUserDefaultsSetValue_shouldBeCalled_whenSetValueCalled() {
        tested.setValue(nil, forKey: "test")
        
        expect(self.userDefaults.setValueCalled).to(beTrue())
    }
    
    func testUserDefaultsValueForKey_shouldBeCalled_whenValueForKeyCalled() {
        _ = tested.value(forKey: "test")
        
        expect(self.userDefaults.valueForKeyCalled).to(beTrue())
    }
}
