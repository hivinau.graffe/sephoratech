// Copyright © Hivinau GRAFFE. All rights reserved.

import Nimble
import XCTest
@testable import Data

final class NumberFormatterWrapperTests: XCTestCase {

    // MARK: - Private properties
    
    private var tested: NumberFormatterWrapperAdapter!
    private let numberFormatter = NumberFormatterMock()

    // MARK: - Lifecycle

    override func setUp() {
        tested = NumberFormatterWrapperAdapter(numberFormatter: numberFormatter)
        super.setUp()
    }
    
    // MARK: - Tests
    
    func testNumberFormatterUsesGroupingSeparator_shouldBeSet_whenUsesGroupingSeparatorDidSet() {
        tested.usesGroupingSeparator = true
        
        expect(self.numberFormatter.usesGroupingSeparatorDidSetCalled).to(beTrue())
    }
    
    func testNumberFormatterNumberStyle_shouldBeSet_whenNumberStyleDidSet() {
        tested.numberStyle = .currency
        
        expect(self.numberFormatter.numberStyleDidSetCalled).to(beTrue())
    }
    
    func testNumberFormatterLocale_shouldBeSet_whenLocaleDidSet() {
        tested.locale = .current
        
        expect(self.numberFormatter.localeDidSetCalled).to(beTrue())
    }
    
    func testNumberFormatterStringFromNumberCalled_shouldBeCalled_whenStringFromValueCalled() {
        _ = tested.string(from: 2)
        
        expect(self.numberFormatter.stringFromNumberCalled).to(beTrue())
    }
}
