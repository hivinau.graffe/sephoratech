// Copyright © Hivinau GRAFFE. All rights reserved.

import Nimble
import UIKit
import XCTest
@testable import Data

final class UIWindowWrapperTests: XCTestCase {

    // MARK: - Private properties
    
    private var tested: UIWindowWrapperAdapter!
    private let window = UIWindowMock()

    // MARK: - Lifecycle

    override func setUp() {
        tested = UIWindowWrapperAdapter(window: window)
        super.setUp()
    }
    
    // MARK: - Tests
    
    func testWindowMakeKeyAndVisible_shouldBeCalled_whenMakeKeyAndVisibleCalled() {
        tested.makeKeyAndVisible()
        
        expect(self.window.makeKeyAndVisibleCalled).to(beTrue())
    }
    
    func testRaw_shouldReturnWindow() {
        expect(self.tested.raw()).to(be(self.window))
    }
    
    func testWindowRootViewController_shouldBeSet_whenRootViewControllerSet() {
        let viewController = UIViewController()
        
        tested.rootViewController = viewController
        
        expect(self.window.rootViewController).to(equal(viewController))
    }
    
    func testRootViewController_shouldEqualWindowRootViewController() {
        let viewController = UIViewController()
        
        window.rootViewController = viewController
        
        expect(self.tested.rootViewController).to(equal(viewController))
    }
}
