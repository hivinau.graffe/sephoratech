// Copyright © Hivinau GRAFFE. All rights reserved.

import Nimble
import XCTest
@testable import Data

@available(iOS, introduced: 13.0, deprecated)
final class URLSessionDataTaskWrapperTests: XCTestCase {

    // MARK: - Private properties
    
    private var tested: URLSessionDataTaskWrapperAdapter!
    private let sessionDataTask = URLSessionDataTaskMock()
    
    // MARK: - Lifecycle
    
    override func setUp() {
        tested = URLSessionDataTaskWrapperAdapter(sessionDataTask: sessionDataTask)
        super.setUp()
    }
    
    // MARK: - Tests
    
    func testSessionDataTaskResume_shouldBeCalled_whenResumeCalled() {
        tested.resume()
        
        expect(self.sessionDataTask.resumeCalled).to(beTrue())
    }
    
    func testSessionDataTaskCancel_shouldBeCalled_whenCancelCalled() {
        tested.cancel()
        
        expect(self.sessionDataTask.cancelCalled).to(beTrue())
    }
    
    func testRaw_shouldReturnSessionDataTask() {
        expect(self.tested.raw()).to(be(self.sessionDataTask))
    }
}
