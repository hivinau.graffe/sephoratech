// Copyright © Hivinau GRAFFE. All rights reserved.

import Nimble
import XCTest
@testable import Data

@available(iOS, introduced: 13.0, deprecated)
final class URLSessionDataTaskWrapperFactoryTests: XCTestCase {

    // MARK: - Private properties
    
    private var tested: URLSessionDataTaskWrapperFactory!
    private let sessionDataTask = URLSessionDataTaskMock()
    
    // MARK: - Lifecycle
    
    override func setUp() {
        tested = URLSessionDataTaskWrapperFactory()
        super.setUp()
    }
    
    // MARK: - Tests
    
    func testBuildFrom_shouldReturnSessionDataTaskWrapper() {
        let wrapper = tested.build(from: sessionDataTask)
        
        expect(wrapper.raw()).to(be(self.sessionDataTask))
    }
}
