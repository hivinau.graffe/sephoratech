// Copyright © Hivinau GRAFFE. All rights reserved.

import Nimble
import XCTest
@testable import Data

final class JSONEncoderWrapperTests: XCTestCase {

    // MARK: - Private properties
    
    private var tested: JSONEncoderWrapperAdapter!
    private let jsonEncoder = JSONEncoderMock()
    private let dataMocker = DataMocker()

    // MARK: - Lifecycle

    override func setUp() {
        tested = JSONEncoderWrapperAdapter(jsonEncoder: jsonEncoder)
        super.setUp()
    }
    
    // MARK: - Tests
    
    func testJsonEncoderEncodeEncodableFromData_shouldBeCalled_whenEncodeEncodableCalled() throws {
        let encodable = EncodableMock()
        jsonEncoder.dataMock = dataMocker.data(from: "test")
        
        _ = try tested.encode(encodable)
        
        expect(self.jsonEncoder.encodeEncodableFromDataCalled).to(beTrue())
    }
}
