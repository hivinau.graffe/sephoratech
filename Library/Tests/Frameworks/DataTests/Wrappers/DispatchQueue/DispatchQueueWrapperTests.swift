// Copyright © Hivinau GRAFFE. All rights reserved.

import Nimble
import XCTest
@testable import Data

final class DispatchQueueWrapperTests: XCTestCase {

    // MARK: - Private properties
    
    private var tested: DispatchQueueWrapperAdapter!
    private let dispatcher = DispatcherMock()

    // MARK: - Lifecycle

    override func setUp() {
        tested = DispatchQueueWrapperAdapter(dispatcher: dispatcher)
        super.setUp()
    }
    
    // MARK: - Tests
    
    func testDispatcherAsyncExecute_shouldBeCalled_whenAsyncExecuteCalled() {
        tested.async {}
        
        dispatcher.verify(.async(group: .any, qos: .any, flags: .any, execute: .any))
    }
    
    func testDispatcherAsDispatchQueue_shouldBeCalled_whenRawCalled() {
        dispatcher.given(.asDispatchQueue(getter: .main))
        
        _ = tested.raw()
        
        dispatcher.verify(.asDispatchQueue)
    }
}
