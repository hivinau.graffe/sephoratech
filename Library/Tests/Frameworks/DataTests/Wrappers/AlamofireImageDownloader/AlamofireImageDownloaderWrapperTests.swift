// Copyright © Hivinau GRAFFE. All rights reserved.

import Alamofire
import Nimble
import XCTest
@testable import Data

final class AlamofireImageDownloaderWrapperTests: XCTestCase {

    // MARK: - Private properties
    
    private var tested: AlamofireImageDownloaderWrapperAdapter!
    private let imageDownloader = AFImageDownloaderMock()
    private let requestMocker = URLRequestMocker()

    // MARK: - Lifecycle

    override func setUp() {
        tested = AlamofireImageDownloaderWrapperAdapter(imageDownloader: imageDownloader)
        super.setUp()
    }
    
    // MARK: - Tests
    
    func testDownloadImageRequestCompletion_shouldBeCalled_whenImageDownloaderDownloadCompletionWithSuccessImageCalled() throws {
        let request = requestMocker.request()
        let expectedImage = UIImage()
        imageDownloader.imageResponse = .init(request: nil,
                                              response: nil,
                                              data: nil,
                                              metrics: nil,
                                              serializationDuration: .zero,
                                              result: .success(expectedImage))
        var image: UIImage?
        
        tested.downloadImage(request!, completion: {
            image = $0
        })
        
        expect(image).toEventually(equal(expectedImage))
    }
    
    func testDownloadImageRequestCompletion_shouldNeverBeCalled_whenImageDownloaderDownloadCompletionWithSuccessImageCalled() throws {
        let request = requestMocker.request()
        imageDownloader.imageResponse = .init(request: nil,
                                              response: nil,
                                              data: nil,
                                              metrics: nil,
                                              serializationDuration: .zero,
                                              result: .failure(.requestCancelled))
        var image: UIImage?
        
        tested.downloadImage(request!, completion: {
            image = $0
        })
        
        expect(image).toEventually(beNil())
    }
}
