// Copyright © Hivinau GRAFFE. All rights reserved.

import Nimble
import XCTest
@testable import Data

final class JSONDecoderWrapperTests: XCTestCase {

    // MARK: - Private properties
    
    private var tested: JSONDecoderWrapperAdapter!
    private let jsonDecoder = JSONDecoderMock()
    private let dataMocker = DataMocker()

    // MARK: - Lifecycle

    override func setUp() {
        tested = JSONDecoderWrapperAdapter(jsonDecoder: jsonDecoder)
        super.setUp()
    }
    
    // MARK: - Tests
    
    func testJsonDecoderDecodeDecodableFromData_shouldBeCalled_whenDecodeDecodableFromDataCalled() throws {
        jsonDecoder.decodableMock = DecodableMock()
        let data = dataMocker.data(from: "test")
        
        _ = try tested.decode(DecodableMock.self, from: data!)
        
        expect(self.jsonDecoder.decodeDecodableFromDataCalled).to(beTrue())
    }
}
